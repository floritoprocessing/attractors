// preset colors:
//preset[0]=new Col_16bit(50059,60764,37653);  // electric green
//preset[1]=new Col_16bit(46842,58160,57536);  // white/blue/green
//preset[2]=new Col_16bit(39906,48714,63106);  // cold blue
//preset[3]=new Col_16bit(56234,37953,41757);  // firework (purple,yellow)
//preset[4]=new Col_16bit(36455,60382,48035);  // stefan green
//preset[5]=new Col_16bit(44891,60616,38805);  // jungle green yellow

Attractor[] att; int nr_att;
Particle[] dot; int nr_dot;
int mainTypes; Col_16bit dotCol; Col_16bit[] dotColVar;
Col_16bit[] bgBuf; int maxpix;
int W,H;
int keyInit;
int frameCount;

void setup() {
  size(400,300,P3D); 
  background(0); 
  //framerate(25); 
  W=width; H=height; maxpix=W*H; frameCount=0; keyInit=0;
  
  mainTypes=6; 
  // setup color and color variations:
  dotColVar=new Col_16bit[mainTypes]; 
  dotCol=new Col_16bit((int)random(65536/2,65536),(int)random(65536/2,65536),(int)random(65536/2,65536)); dotCol.toPrint();
  for (int i=0;i<mainTypes;i++) {dotColVar[i]=variateColor(dotCol,50*256);}
  
  // setup attractors and particles
  nr_att=50; initAttractors();
  nr_dot=5000; initParticles();
  
  // setup bgbuf;
  bgBuf=new Col_16bit[maxpix]; bgBuf=fillBuf(bgBuf,new Col_16bit(0,0,0));
  
  println("press [c] to change color");
  println("press [a] to change attractors");
  println("press [p] to change particles");
}

void initAttractors() {
  att=new Attractor[nr_att]; for (int i=0;i<nr_att;i++) {att[i]=new Attractor(); att[i].randomize();}
}
void initParticles() {
  dot=new Particle[nr_dot]; for (int i=0;i<nr_dot;i++) {dot[i]=new Particle(); dot[i].randomize();}
}



void keyPressed() {
  if (key=='c') {keyInit=1;}
  if (key=='a') {keyInit=2;}
  if (key=='p') {keyInit=3;}
}
void draw() {
  // keypressed changes:
  if (keyInit!=0) {
    if (keyInit==1) {
      dotCol=new Col_16bit((int)random(65536/2,65536),(int)random(65536/2,65536),(int)random(65536/2,65536));
      dotCol.toPrint();
      for (int i=0;i<mainTypes;i++) {dotColVar[i]=variateColor(dotCol,50*256);}
      for (int i=0;i<nr_dot;i++) {
        dot[i].updateDrawingColor();
      }
    }
    if (keyInit==2) {initAttractors();}    
    if (keyInit==3) {initParticles();}
    keyInit=0;
  }
  
  //drawing:
  frameCount++;
  background(255);
  bgBufFade(0.995);
  drawbgBuf();
  for (int i=0;i<nr_att;i++) {
    att[i].update(); 
  }
  for (int i=0;i<nr_dot;i++) {
    dot[i].update();
    dot[i].onScreen();
  }
}



class Particle {
  float x,y;           // screen position
  float xm=0, ym=0;    // move vector
  int type;            // type (defines how it reacts to attractor, and defines color)
  Col_16bit draw_col;  // drawing color
  
  Particle() {
  }
  
  void randomize() {  // randomize type, color and position
    type=(int)random(mainTypes);
    draw_col=dotColVar[type];
    x=random(100); y=random(100);
  }
  
  void updateDrawingColor() {
    draw_col=dotColVar[type];
  }
  
  void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      float typeRel=abs(type-att[i].type)/((float)mainTypes-1)-0.5;
      if (d2>0.005) { xm+=typeRel*att[i].speed*dx/d2; ym+=typeRel*att[i].speed*dy/d2; }
    }
    xm*=0.9; ym*=0.9;
    x+=xm; y+=ym; 
  }
  
  void onScreen() {
    setOnBg(int(x*W/100.0),int(y*H/100.0),draw_col);
  }
}



class Attractor {
  float x,y;
  int type;
  float frq,pha;
  float spd=4, speed;
  float screenR;
  Attractor() {
    speed=spd;
    screenR=W/80;
  }
  void randomize() {
    type=(int)random(mainTypes);
    frq=random(0.05,0.30); pha=random(TWO_PI);
    x=random(100); y=random(100);
  }
  void update() {
    speed=spd*sin(frq*frameCount/25.0+pha);//
  }
}



void setOnBg(int x, int y, Col_16bit c) {
  float p1=0.1; float p2=1.0-p1;
  if (x>=0&&x<W&&y>=0&&y<H) {
    int i=y*W+x;
    float r1=c.getRed(), g1=c.getGreen(), b1=c.getBlue();
    float r2=bgBuf[i].getRed(), g2=bgBuf[i].getGreen(), b2=bgBuf[i].getBlue();
    float rr=p1*r1+p2*r2, gg=p1*g1+p2*g2, bb=p1*b1+p2*b2;
    bgBuf[i]=new Col_16bit(rr,gg,bb);
  }
}

void bgBufFade(float val) {
  for (int i=0;i<maxpix;i++) {
    float rr=bgBuf[i].getRed();
    float gg=bgBuf[i].getGreen();
    float bb=bgBuf[i].getBlue();
    rr=rr*val;
    gg=gg*val;
    bb=bb*val;
    bgBuf[i]=new Col_16bit(rr,gg,bb);
  }
}

void drawbgBuf() {
  for (int x=0;x<W;x++) {
    for (int y=0;y<H;y++) {
      int i=y*W+x;
      set(x,y,bgBuf[i].toNormRGB());
    }
  }
}

Col_16bit[] fillBuf(Col_16bit[] buf,Col_16bit c) {
  buf=bgBuf;
  for (int i=0;i<bgBuf.length;i++) {
    buf[i]=c;
  }
  return buf;
}

class Col_16bit {
  float rr,gg,bb;
  Col_16bit(float r, float g, float b) {
    rr=r; gg=g; bb=b;
  }
  float getRed() {
    return rr;
  }
  float getGreen() {
    return gg;
  }
  float getBlue() {
    return bb;
  }
  int toNormRGB() {
    int r8=int(constrain(rr/256.0,0,255));
    int g8=int(constrain(gg/256.0,0,255));
    int b8=int(constrain(bb/256.0,0,255));
    return (r8<<16|g8<<8|b8);
  }
  void toPrint() {
    println("preset[x]=new Col_16bit("+int(rr)+","+int(gg)+","+int(bb)+");  // ");
  }
}

Col_16bit[] variateColors(Col_16bit ttc,int imax) {
  Col_16bit[] outcol=new Col_16bit[imax];
  for (int i=0;i<imax;i++) {
    outcol[i]=variateColor(ttc,255*50);
  }
  return outcol;
}

Col_16bit variateColor(Col_16bit tc,float coff) {
  float rr=constrain(tc.getRed()+random(-coff,coff),0,65535);
  float gg=constrain(tc.getGreen()+random(-coff,coff),0,65535);
  float bb=constrain(tc.getBlue()+random(-coff,coff),0,65535);
  return (new Col_16bit(rr,gg,bb));
}
