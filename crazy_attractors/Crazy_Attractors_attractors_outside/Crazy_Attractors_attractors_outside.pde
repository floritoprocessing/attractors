float gScale;
float xMax, yMax;
boolean paused=false;

void setup() {
  size(1200,900,P3D);
  colorMode(RGB,255);
  background(255,255,255);
  gScale=(width>height?width:height);
  xMax=width/gScale; yMax=height/gScale; println(xMax+" "+yMax);
  initAttractors();
  initParticles();
}

void draw() {
  //background(255,255,255);
  //showAttractors();
  for (int i=0;i<20;i++) {
    if (!paused) { updateParticles(); } 
  }
  println(millis());
}

void keyPressed() { if (key=='p') {paused=!paused; println(" pause: "+paused);} }

// --------------------------------
// PARTICLES

int nrOfParticles=5000;
Particle[] prt=new Particle[nrOfParticles];

void initParticles() { for (int i=0;i<nrOfParticles;i++) {prt[i]=new Particle();}}
void updateParticles() { for (int i=0;i<nrOfParticles;i++) {prt[i].update();}}

class Particle {
  // position:
  float x=random(xMax), y=random(yMax);
  
  // movement:
  float xmov=random(1)<0.5?random(0.001,0.005):-random(0.001,0.005);
  float ymov=random(1)<0.5?random(0.001,0.005):-random(0.001,0.005);
  
  Particle() {  }
  
  void update() {
    
    for (int a=0;a<nrOfAttractors;a++) {
      float dx=att[a].x-x;
      float dy=att[a].y-y;
//      float d=dist(x,y,att[a].x,att[a].y);
      float d2=sq(dx)+sq(dy);
  
      // gravity:
      if (att[a].gravity) {
        if (d2>0.005*0.005) {
          // gravity works only if going towards the object
          float angle=acos(dx*xmov+dy*ymov);
          if (abs(angle)>HALF_PI) {
            float fGrav=0.00001*att[a].grav;
            xmov+=dx*fGrav/d2;
            ymov+=dy*fGrav/d2;
          }
        }
      }
      
      // breaking:
      if (att[a].breaking) {
        if (d2>0.005*0.005&&d2<att[a].range2) {
          xmov*=att[a].breakForce;
          ymov*=att[a].breakForce;
        } 
      }
    }
    
    //xmov*=0.999; ymov*=0.999;
    
    x+=xmov; if (x>=xMax) {x-=xMax;}; if (x<0) {x+=xMax;}
    y+=ymov; if (y>=yMax) {y-=yMax;}; if (y<0) {y+=yMax;}
    sSet(x*gScale,y*gScale,color(0,0,0),0.05);
  }
  
}


// --------------------------------
// ATTRACTORS

int nrOfAttractors=20*9;
Attractor[] att=new Attractor[nrOfAttractors];

void initAttractors() { 
  for (int i=0;i<nrOfAttractors/9;i++) {
    int iBase=i*9+4;
    att[iBase]=new Attractor();
    for (int xo=-1;xo<=1;xo++) { for (int yo=-1;yo<=1;yo++) {
      int j=i*9+(xo+1)*3+(yo+1);
      if (j!=iBase) {
        att[j]=new Attractor();
        att[j].x=att[iBase].x+xo*xMax;
        att[j].y=att[iBase].y+yo*yMax;
        att[j].range=att[iBase].range;
        att[j].range=att[iBase].range2;
        att[j].type=att[iBase].type;
        att[j].gravity=att[iBase].gravity;
        att[j].breaking=att[iBase].breaking;
        att[j].grav=att[iBase].grav;
        att[j].breakForce=att[iBase].breakForce;
      }
    }}
  }
}
void showAttractors() { for (int i=0;i<nrOfAttractors;i++) {att[i].show();}}

class Attractor {
  // position:
  float x=random(xMax), y=random(yMax);
  float range=random(0.1,0.5); // for speedup as well as gravity
  float range2=sq(range);
  
  // type:
  int type=(int)random(4); 
  boolean gravity=((type&1)==1);
  boolean breaking=((type>>1&1)==1);
  
  // bit 0 of type: gravity push?pull? yes/no
  float grav=random(-1,1);
  
  // bit 1 of type: break/speedup yes/no
  float breakForce=random(1)<0.5?random(0.97,0.999):1/random(0.97,0.999);
   
  Attractor() { }
  
  void show() {
    // bit0: gravity yes/no
    
//    stroke(0);
//    noFill();
//    ellipse((1+x)*gScale/3.0,(1+y)*gScale/3.0,range*gScale/3.0,range*gScale/3.0);
    
    
    if (gravity) {
      stroke(0);
      if (grav<0) { fill(255,0,0,64); } else { fill(0,255,0,64); }
      ellipseMode(CENTER);
      ellipse(x*gScale,y*gScale,0.01*gScale,0.01*gScale);
      stroke(255,0,0,32); noFill();
      ellipse(x*gScale,y*gScale,range*gScale,range*gScale);
    }
    
    // bit1: break/speedup
    if (breaking) {
      stroke(0);
      if (breakForce<1) { fill(255,0,0,16); } else { fill(0,0,255,16); }
      ellipseMode(CENTER);
      ellipse(x*gScale,y*gScale,range*gScale,range*gScale);
    }
    
    
  }
}






void sSet(float _x, float _y, int c, float p) {
  int x=int(_x); int y=int(_y);
  int bg=get(x,y);
  set(x,y,mix(c,bg,p));
//  set(x,y,c);
}

color mix(int a, int b, float p) {
  float q=1.0-p;
  int rr=int(ch_red(a)*p+ch_red(b)*q);
  int gg=int(ch_grn(a)*p+ch_grn(b)*q);
  int bb=int(ch_blu(a)*p+ch_blu(b)*q);
  return color(rr,gg,bb);
}

int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }
