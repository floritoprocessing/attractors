// preset colors:
// preset[x]=new Col_16bit(5028,22876,27789);  //  nice blue
// preset[x]=new Col_16bit(8139,7963,21750);  //  purple blue dark ink
// preset[x]=new Col_16bit(14245,20093,687);  //  jungly green
// preset[x]=new Col_16bit(27931,12559,24505);  // nice wine red

int mainTypes; 
Attractor[] att; int nr_att;
Particle[] dot; int nr_dot;
Col_16bit dotCol; Col_16bit[] dotColVar;
Col_16bit[] bgBuf; 
int W,H,W2,H2; int maxpix; float aspRat;

// zooming variables:
float globalZoom; boolean goHigherZoom; float xZoomOffset, yZoomOffset; 
boolean drawZoomSquare; float zoomSquareX1,zoomSquareY1,zoomSquareX2,zoomSquareY2;


void setup() {
  size(400,300,P3D);
  background(255); 
  //framerate(25); 
  W=width; H=height; W2=W/2; H2=H/2; maxpix=W*H; aspRat=W/(float)H;
  // zooming setup:
  globalZoom=1; goHigherZoom=false; xZoomOffset=50; yZoomOffset=50; 
  drawZoomSquare=false; zoomSquareX1=0; zoomSquareY1=0; zoomSquareX2=0; zoomSquareY2=0; rectMode(CORNERS); noFill(); stroke(128);
  
  mainTypes=12; 
  // setup color and color variations:
  dotColVar=new Col_16bit[mainTypes]; 
  dotCol=new Col_16bit((int)random(0,65536/2),(int)random(0,65536/2),(int)random(0,65536/2)); dotCol.toPrint();
  for (int i=0;i<mainTypes;i++) {dotColVar[i]=variateColor(dotCol,40*256);}
  
  // setup attractors and particles
  nr_att=32; initAttractors();
  nr_dot=5000; initParticles();
  
  // setup bgbuf;
  bgBuf=new Col_16bit[maxpix]; bgBuf=fillBuf(bgBuf,new Col_16bit(65535,65535,65535));
}

void initAttractors() {
  att=new Attractor[nr_att]; for (int i=0;i<nr_att;i++) {att[i]=new Attractor(); att[i].randomize();}
}
void initParticles() {
  dot=new Particle[nr_dot]; for (int i=0;i<nr_dot;i++) {dot[i]=new Particle(); dot[i].randomize();}
}



void mousePressed() {
  drawZoomSquare=true;
  zoomSquareX1=mouseX; zoomSquareY1=mouseY;
}
void mouseReleased() {
  drawZoomSquare=false;
  float zoomFac=abs(W/(zoomSquareX1-mouseX));
  xZoomOffset=xZoomOffset + 100*((zoomSquareX1+zoomSquareX2)/2.0-W2)/(float)W/globalZoom;
  yZoomOffset=yZoomOffset + 100*((zoomSquareY1+zoomSquareY2)/2.0-H2)/(float)H/globalZoom;
  print("xoff: "+xZoomOffset);
  print(" | yoff: "+yZoomOffset+" | ");
  globalZoom*=zoomFac; println(zoomFac);
  goHigherZoom=true;
}

void draw() {
  if (goHigherZoom) {
    goHigherZoom=false;
    bgBuf=fillBuf(bgBuf,new Col_16bit(65535,65535,65535));
    background(255);
  }
  //drawing:
  bgBufFade(0.995);
  drawbgBuf();
  for (int i=0;i<nr_att;i++) {
    att[i].update(); 
    //att[i].onScreen();
  }
  for (int i=0;i<nr_dot;i++) {
    dot[i].update();
    dot[i].onScreen();
    //dot[i].onScreenDot();
  }
  if (drawZoomSquare) {
    float d=dist(zoomSquareX1,zoomSquareY1,mouseX,mouseY);
    float dx=d*((mouseX<zoomSquareX1)?-1:1);
    float dy=d*((mouseY<zoomSquareY1)?-1:1);
    zoomSquareX2=zoomSquareX1+dx;
    zoomSquareY2=zoomSquareY1+dy/aspRat;
    rect(zoomSquareX1,zoomSquareY1,zoomSquareX2,zoomSquareY2);
  }
}






class Particle {
  float x,y;           // screen position
  float xm=0, ym=0;    // move vector
  int type;            // type (defines how it reacts to attractor, and defines color)
  float typePerc;
  float[] typeRel1;
  Col_16bit draw_col;  // drawing color
  
  Particle() {
  }
  
  void randomize() {  // randomize type, color and position
    type=(int)random(mainTypes); typePerc=type/((float)mainTypes-1);
    draw_col=dotColVar[type];
    x=random(-50,150); y=random(-50,150);
    typeRel1=new float[nr_att];
    for (int i=0;i<nr_att;i++) {
      typeRel1[i]=abs((type-att[i].type)/((float)mainTypes-1))*att[i].speed;
    }
  }
  
  void updateDrawingColor() {
    draw_col=dotColVar[type];
  }
  
  void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      if (d2!=0) {
        float typeRel=typeRel1[i]/d2;
        if (d2<500) {typeRel*=-1;}
        xm+=typeRel*dx; ym+=typeRel*dy;
      }
    }
    float damp=0.96+typePerc*0.02;
    xm*=damp; ym*=damp;
    x+=xm; y+=ym; 
  }
  
  void onScreen() {
    setOnBg(int(globalZoom*W*(x-xZoomOffset)/100.0+W2),int(globalZoom*H*(y-yZoomOffset)/100.0+H2),draw_col);
  }
//  void onScreenDot() {
//    set(int(x*W/100.0),int(y*H/100.0),draw_col.toNormRGB());
//  }
}



  



class Attractor {
  float x,y,xm,ym;
  int type;
  float frq,pha;
  float spd=1, speed;
  float screenR;
  Attractor() {
    speed=spd;
    screenR=W/80;
  }
  void randomize() {
    type=(int)random(mainTypes);
    frq=random(0.4,2.4); pha=random(TWO_PI);
    x=random(100); y=random(100);
    float dir=atan2(50-y,50-x);
    if (random(1)<0.5) {dir+=random(0.8,1.2)*HALF_PI;} else {dir-=random(0.8,1.2)*HALF_PI;}
    xm=0.1*cos(dir); ym=0.1*sin(dir);
  }
  void update() {
    float dx=50-x, dy=50-y;
    float d2=sq(dx)+sq(dy);
    xm+=0.01*(dx/d2); ym+=0.01*(dy/d2);
    x+=xm; y+=ym;
    speed=spd;//*sin(frq*frameCount/25.0+pha);//
  }
//  void onScreen() {
//    noStroke();
//    fill(255,0,0); ellipseMode(CENTER_DIAMETER); 
//    ellipse(x*W/100.0,y*H/100.0,screenR,screenR);
//  }
}



void setOnBg(int x, int y, Col_16bit c) {
  float p1=0.05; float p2=1.0-p1;
  if (x>=0&&x<W&&y>=0&&y<H) {
    int i=y*W+x;
    float r1=c.getRed(), g1=c.getGreen(), b1=c.getBlue();
    float r2=bgBuf[i].getRed(), g2=bgBuf[i].getGreen(), b2=bgBuf[i].getBlue();
    float rr=p1*r1+p2*r2, gg=p1*g1+p2*g2, bb=p1*b1+p2*b2;
    bgBuf[i]=new Col_16bit(rr,gg,bb);
  }
}

void bgBufFade(float val) {
  for (int i=0;i<maxpix;i++) {
    float rr=65535-bgBuf[i].getRed();
    float gg=65535-bgBuf[i].getGreen();
    float bb=65535-bgBuf[i].getBlue();
    rr=65535-rr*val;
    gg=65535-gg*val;
    bb=65535-bb*val;
    bgBuf[i]=new Col_16bit(rr,gg,bb);
  }
}

void drawbgBuf() {
  for (int x=0;x<W;x++) {
    for (int y=0;y<H;y++) {
      int i=y*W+x;
      set(x,y,bgBuf[i].toNormRGB());
    }
  }
}

Col_16bit[] fillBuf(Col_16bit[] buf,Col_16bit c) {
  buf=bgBuf;
  for (int i=0;i<bgBuf.length;i++) {
    buf[i]=c;
  }
  return buf;
}




class Col_16bit {
  float rr,gg,bb;
  Col_16bit(float r, float g, float b) {
    rr=r; gg=g; bb=b;
  }
  float getRed() {
    return rr;
  }
  float getGreen() {
    return gg;
  }
  float getBlue() {
    return bb;
  }
  int toNormRGB() {
    int r8=int(constrain(rr/256.0,0,255));
    int g8=int(constrain(gg/256.0,0,255));
    int b8=int(constrain(bb/256.0,0,255));
    return (r8<<16|g8<<8|b8);
  }
  void toPrint() {
    println("preset[x]=new Col_16bit("+int(rr)+","+int(gg)+","+int(bb)+");  // ");
  }
}

Col_16bit[] variateColors(Col_16bit ttc,int imax) {
  Col_16bit[] outcol=new Col_16bit[imax];
  for (int i=0;i<imax;i++) {
    outcol[i]=variateColor(ttc,255*50);
  }
  return outcol;
}

Col_16bit variateColor(Col_16bit tc,float coff) {
  float rr=constrain(tc.getRed()+random(-coff,coff),0,65535);
  float gg=constrain(tc.getGreen()+random(-coff,coff),0,65535);
  float bb=constrain(tc.getBlue()+random(-coff,coff),0,65535);
  return (new Col_16bit(rr,gg,bb));
}
