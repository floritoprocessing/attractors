/*
Attractors IV
created in 2005 by Marcus Graf
marcus[AT]florito.net

// click on screen to create new dots
// press [i] to restart
// press [p] to toggle pause

*/

// preset colors:
// preset[x]=new color(5028/256.0,22876/256.0,27789/256.0);  //  nice blue
// preset[x]=new color(8139/256.0,7963/256.0,21750/256.0);  //  purple blue dark ink
// preset[x]=new color(14245/256.0,20093/256.0,687/256.0);  //  jungly green
// preset[x]=new color(27931/256.0,12559/256.0,24505/256.0);  // nice wine red

int nr_att=16;
int max_nr_dot=5000;
int splotSize=500;
int mainTypes=12; 

Particle[] dot; int dotToSteal; int nr_dot=0;
color dotCol; color[] dotColVar;

int W,H,W2,H2; int maxpix; float xToW,yToH;

boolean newSplot=false;
boolean initializeAll=true;
boolean paused=false;

void setup() {
  size(400,300,P3D); W=width; H=height; xToW=1.2*W/100.0; yToH=1.2*H/100.0; W2=W/2; H2=H/2; maxpix=W*H;
  println("[i] to initialize all");
  println("[p] to toggle pause");
  println("press mouse to add new particles");
}


void draw() {
  if (initializeAll) {
    initializeAll=false;
    background(255);
    setupColors();
    setupAttractors();
    setupDots();
    setupBgBuf();
    setupDissolve();
    nr_dot=0;
  }
  
  if (!paused) {
    if (newSplot) {  newSplot=false;
      createNewSplot(); //println("dots: "+nr_dot);
    }
    
//    bgBufCounter++; if (bgBufCounter%3==0) { bgBufFade(0.985074875,0,1); }
    
    bgBufCounter++; 
    float steps=50.0;
    float from=(bgBufCounter%steps)/steps;
    float to=((bgBufCounter%steps)+1)/steps;
    bgBufFadeDissolve(0.95,from,to);

//    bgBufFade(0.995,0,1);
    
   
    drawbgBuf();
    updateAttractors();
    updateAndDrawDots();
  }
}

void mousePressed() {
  newSplot=true;
}

void keyPressed() { 
  if (key=='i') {initializeAll=true; paused=false;} 
  if (key=='p') {paused=!paused;}
}




void createNewSplot() {
  for (int i=0;i<splotSize;i++) {
    dot[dotToSteal].x=50+(mouseX-W2)/xToW; dot[dotToSteal].y=50+(mouseY-H2)/yToH;
    dot[dotToSteal].xm=random(-0.1,0.1); dot[dotToSteal].ym=random(-0.1,0.1);
    nr_dot++; if (nr_dot>max_nr_dot) {nr_dot=max_nr_dot;}
    dotToSteal++; if (dotToSteal==nr_dot) {dotToSteal=0;}
  }
}







// PARTICLES

void setupDots() { dot=new Particle[max_nr_dot]; for (int i=0;i<max_nr_dot;i++) {dot[i]=new Particle(); dot[i].randomize();} dotToSteal=0; }
void updateAndDrawDots() { for (int i=0;i<nr_dot;i++) { dot[i].update(); dot[i].onScreen(); } }

class Particle {
  float x,y;           // screen position
  float xm=0, ym=0;    // move vector
  int type;            // type (defines how it reacts to attractor, and defines color)
  float typePerc;      // type, but rescaled to 0..1 (type/mainTypes);
  float damp;
  float[] typeRel1;    //
  color draw_col;  // drawing color
  
  Particle() {
  }
  
  void randomize() {  // randomize type, color and position
    type=(int)random(mainTypes); 
    typePerc=type/((float)mainTypes-1);
    damp=0.965+typePerc*0.015;
    draw_col=dotColVar[type];
    x=random(-50,150); y=random(-50,150);
    typeRel1=new float[nr_att];
    for (int i=0;i<nr_att;i++) {
      typeRel1[i]=(abs((type-att[i].type)/((float)mainTypes-1))-0.1)*att[i].speed;
    }
  }
  
  void updateDrawingColor() { draw_col=dotColVar[type]; }
  
  void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      if (d2!=0) {
        float typeRel=typeRel1[i]/d2;
        if (d2<500) {typeRel*=-1;}
        xm+=typeRel*dx; ym+=typeRel*dy;
      }
    }
    xm*=damp; ym*=damp;
    x+=xm; y+=ym; 
  }
  
  void onScreen() { setOnBg(int(W2+(x-50)*xToW),int(H2+(y-50)*yToH),draw_col); }
}







// ATTRACTORS

Attractor[] att;

void setupAttractors() { att=new Attractor[nr_att]; for (int i=0;i<nr_att;i++) {att[i]=new Attractor(); att[i].randomize();} }

void updateAttractors() { for (int i=0;i<nr_att;i++) { att[i].update(); } }

class Attractor {
  float x,y,xm,ym;
  int type;
  float frq,pha;
  float speed=1;
  Attractor() {
  }
  void randomize() {
    type=(int)random(mainTypes);
    x=random(100); y=random(100);
    float dir=atan2(50-y,50-x);
    if (random(1)<0.5) {dir+=random(0.8,1.2)*HALF_PI;} else {dir-=random(0.8,1.2)*HALF_PI;}
    xm=0.1*cos(dir); ym=0.1*sin(dir);
  }
  void update() {
    float dx=50-x, dy=50-y;
    float d2=sq(dx)+sq(dy);
    xm+=0.01*(dx/d2); ym+=0.01*(dy/d2);
    x+=xm; y+=ym;
  }
}







// BACKGROUND DRAWING FUNCTIONS

Color16bit[] bgBuf; 
int bgBufCounter=0;

void setupBgBuf() { 
  bgBuf=new Color16bit[maxpix]; 
  for (int i=0;i<bgBuf.length;i++) { bgBuf[i]=new Color16bit(color(255,255,255)); }
}

void setOnBg(int x, int y, color c) {
  float p1=0.05; float p2=0.95;  //1.0-p1;
  if (x>=0&&x<W&&y>=0&&y<H) {
    int i=y*W+x;
    float r1=red(c), g1=green(c), b1=blue(c);
    float r2=bgBuf[i].r, g2=bgBuf[i].g, b2=bgBuf[i].b;
    float rr=p1*r1+p2*r2, gg=p1*g1+p2*g2, bb=p1*b1+p2*b2;
    bgBuf[i].setColor(rr,gg,bb);
  }
}

void bgBufFade(float val, float _a, float _b) { 
  int a=int(_a*maxpix); int b=int(_b*maxpix);
  for (int i=a;i<b;i++) { bgBuf[i]=fade(bgBuf[i],val); } 
}

int[] randomPos;
void setupDissolve() {
  // setup array:
  randomPos=new int[maxpix]; for (int i=0;i<maxpix;i++) { randomPos[i]=i; }
  // shuffle array:
  for (int i=0;i<maxpix*2;i++) {
    int i1=(int)random(maxpix), i2=(int)random(maxpix), temp=randomPos[i1];
    randomPos[i1]=randomPos[i2]; randomPos[i2]=temp;
  }
}
void bgBufFadeDissolve(float val, float _a, float _b) {
  int a=int(_a*maxpix); int b=int(_b*maxpix); int i;
  for (int j=a;j<b;j++) { 
    i=randomPos[j];
    bgBuf[i]=fade(bgBuf[i],val); //bgBuf[i].setColor(0,0,0);
  }
}

void drawbgBuf() { for (int x=0;x<W;x++) { for (int y=0;y<H;y++) { set(x,y,bgBuf[y*W+x].toColor()); } } }

class Color16bit {
  float r=0,g=0,b=0;
  Color16bit(color _c) { r=red(_c); g=green(_c); b=blue(_c); }
  Color16bit(float _r,float _g,float _b) { r=_r; g=_g; b=_b; }
  void setColor(float _r,float _g,float _b) { r=_r; g=_g; b=_b; }
  color toColor() { return color(r,g,b); }
}

Color16bit fade(Color16bit inCol,float perc) {  // fade to white 0.999 is slow, 0.001 is super fast
  float rr=255-(255-inCol.r)*perc;
  float gg=255-(255-inCol.g)*perc;
  float bb=255-(255-inCol.b)*perc;
  return new Color16bit(rr,gg,bb);
}




// COLOR FUNCTIONS:

void setupColors() {
  dotColVar=new color[mainTypes]; 
  dotCol=color(random(0,256/2),random(0,256/2),random(0,256/2)); 
  //println("preset[x]=color("+red(dotCol)+","+green(dotCol)+","+blue(dotCol)+");  // ");
  for (int i=0;i<mainTypes;i++) {dotColVar[i]=variateColor(dotCol,40);}
}

color variateColor(color tc,float coff) {
  float rr=constrain(red(tc)+random(-coff,coff),0,255);
  float gg=constrain(green(tc)+random(-coff,coff),0,255);
  float bb=constrain(blue(tc)+random(-coff,coff),0,255);
  return color(int(rr),int(gg),int(bb));
}
