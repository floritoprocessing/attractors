/*
 Attractors IV v Black and White (16bit per channel color, anti-aliasing)
 created in 2005 by Marcus Graf
 marcus[AT]florito.net
 
 // click LEFT MOUSE BUTTON on screen to create new dots
 // press [i] or RIGHT MOUSE BUTTON to restart
 
 */

float particleDrawTime = 0, bufferFadeTime = 0, bufferDrawTime = 0;

int nr_att=16;
int max_nr_dot=6000;
int splotSize=15;
int mainTypes=12; 

Particle[] dot; 
int dotToSteal; 
int nr_dot=0;
GrafColor[] dotColVar;

float xToW,yToH;
boolean initializeAll=true;

BackgroundBuffer backgroundBuffer;

//PFont verdanaBold;
PFont verdanaItalic10;

//PImage arrow;
//String OUT_FILE = "D:\\Stroom Aanvraag\\DVD Stroom\\Sources\\Attractors_iv_BW\\frame";

void setup() {
  size(500,300,P3D); 
  //verdanaBold = loadFont("Verdana-Bold-12.vlw");
  verdanaItalic10 = loadFont("Verdana-Italic-10.vlw");
  textFont(verdanaItalic10,10);
  backgroundBuffer = new BackgroundBuffer(width,height);
  xToW=1.2*width/100.0; 
  yToH=1.2*width/100.0; 
  
  /*
  arrow = loadImage("arrow.jpg");
  arrow.format = ARGB;
  PImage msk = loadImage("arrowMask.jpg");
  arrow.loadPixels();
  for (int i=0;i<arrow.pixels.length;i++) {
    int rgb = arrow.pixels[i]&0xFFFFFF;
    int a = msk.pixels[i]&0xFF;
    arrow.pixels[i]=a<<24|rgb;
  }
  arrow.updatePixels();
  */
}


void draw() {
  //cursor(CROSS);
  int mx = mouseX, my=mouseY;
  if (initializeAll) {
    initializeAll=false;
    setupColors();
    setupAttractors();
    setupDots();
    backgroundBuffer.clear();
    nr_dot=0;
  }

  if (mousePressed&&mouseButton==LEFT) {  
    float dx = pmouseX-mouseX;
    float dy = pmouseY-mouseY;
    float d = sqrt(sq(dx)+sq(dy));
    if (d>1.4) {
      for (float p=0;p<1.0;p+=1/d)
        createNewSplot(mouseX+p*dx,mouseY+p*dy);
    } 
    else
      createNewSplot(mouseX,mouseY);
  }

  long ms = millis();
  backgroundBuffer.fadeToWhite();
  bufferFadeTime = 0.9*bufferFadeTime + 0.1*(millis()-ms);

  ms = millis();
  backgroundBuffer.drawbgBuf();
  bufferDrawTime = 0.9*bufferDrawTime + 0.1*(millis()-ms);

  updateAttractors();

  ms = millis();
  updateAndDrawDots();
  particleDrawTime = 0.9*particleDrawTime + 0.1*(millis()-ms);

  if (keyPressed&&key=='S') {
    //textFont(verdanaBold,12);
    //textAlign(LEFT);
    fill(128);
    println("backgroundBuffer draw time: "+nf(bufferDrawTime,2,1)+" ms");
    println("backgroundBuffer fade time: "+nf(bufferFadeTime,2,1)+" ms");
    println("particle draw time: "+nf(particleDrawTime,2,1)+" ms");
  }

  
  textAlign(RIGHT);
  /*
  if (mouseX>width-80&&mouseY>288&&!mousePressed) {
    cursor(12); 
    fill(128);
  } 
  else {
    cursor(0);
    fill(192);
  }
  */
  
  //text("www.florito.net",width-4,height-4);
  
  for (int x=0;x<width;x++) { set(x,0,0x333333); set(x,height-1,0x333333); }
  for (int x=1;x<width-1;x++) { set(x,1,0x999999); set(x,height-2,0x999999); }
//  for (int x=2;x<width-2;x++) { set(x,2,0xC0C0C0); set(x,height-3,0xc0c0c0); }
  for (int y=0;y<height;y++) { set(0,y,0x333333); set(width-1,y,0x333333); }
  for (int y=1;y<height-1;y++) { set(1,y,0x999999); set(width-2,y,0x999999); }
//  for (int y=2;y<height-2;y++) { set(2,y,0xC0C0C0); set(width-3,y,0xc0c0c0); }

  //image(arrow,mx,my); 
  //saveFrame(OUT_FILE+"_#####.tga");
}

void keyPressed() { 
  if (key=='i') initializeAll=true;
}

void mousePressed() {
  /*
  if (mouseButton==LEFT) {
    if (mouseX>width-80&&mouseY>288) {
      link("http://www.florito.net");//,"_blank");
    }
  }
  */
  if (mouseButton==RIGHT) initializeAll=true;
}




void createNewSplot(float sx, float sy) {
  for (int i=0;i<splotSize;i++) {
    float rad = 0.5 * pow(random(1.0),3);
    float rd = random(TWO_PI);
    dot[dotToSteal].x=50+(sx-width/2.0)/xToW + rad*cos(rd); 
    dot[dotToSteal].y=50+(sy-height/2.)/yToH + rad*sin(rd);
    dot[dotToSteal].xm=0;//0.5*random(-0.1,0.1); 
    dot[dotToSteal].ym=0;//0.5*random(-0.1,0.1);
    dot[dotToSteal].resetLastScreenPos();
    nr_dot++; 
    if (nr_dot>max_nr_dot) {
      nr_dot=max_nr_dot;
    }
    dotToSteal++; 
    if (dotToSteal==nr_dot) {
      dotToSteal=0;
    }
  }
}







// PARTICLES

void setupDots() { 
  dot=new Particle[max_nr_dot]; 
  for (int i=0;i<max_nr_dot;i++) {
    dot[i]=new Particle(backgroundBuffer,width/2,height/2,xToW,yToH); 
    dot[i].randomize();
  } 
  dotToSteal=0; 
}
void updateAndDrawDots() { 
  for (int i=0;i<nr_dot;i++) { 
    dot[i].update(); 
    dot[i].onScreen(); 
  } 
}









// ATTRACTORS

Attractor[] att;

void setupAttractors() { 
  att=new Attractor[nr_att]; 
  for (int i=0;i<nr_att;i++) {
    att[i]=new Attractor(); 
    att[i].randomize();
  } 
}

void updateAttractors() { 
  for (int i=0;i<nr_att;i++) { 
    att[i].update(); 
  } 
}

class Attractor {
  float x,y,xm,ym;
  int type;
  float frq,pha;
  float speed=1;
  Attractor() {
  }
  void randomize() {
    type=(int)random(mainTypes);
    x=random(100); 
    y=random(100);
    float dir=atan2(50-y,50-x);
    if (random(1)<0.5) {
      dir+=random(0.8,1.2)*HALF_PI;
    } 
    else {
      dir-=random(0.8,1.2)*HALF_PI;
    }
    xm=0.1*cos(dir); 
    ym=0.1*sin(dir);
  }
  void update() {
    float dx=50-x, dy=50-y;
    float d2=sq(dx)+sq(dy);
    xm+=0.01*(dx/d2); 
    ym+=0.01*(dy/d2);
    x+=xm; 
    y+=ym;
  }
}





// COLOR FUNCTIONS:

void setupColors() {
  dotColVar=new GrafColor[mainTypes]; 
  int br = (int)random(16,96);
  GrafColor dotCol=new GrafColor(br,br,br); 
  for (int i=0;i<mainTypes;i++) {
    dotColVar[i] = new GrafColor(dotCol);//dotCol);
    dotColVar[i].changeMode(GrafColor.MODE_16BIT);
    dotColVar[i].variateColor(60);
  }
}



