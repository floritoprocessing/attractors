class Particle {
  float x,y;           // screen position
  float xm=0, ym=0;    // move vector
  int type;            // type (defines how it reacts to attractor, and defines color)
  float typePerc;      // type, but rescaled to 0..1 (type/mainTypes);
  float damp;
  float[] typeRel1;    //
  
  GrafColor draw_col;  // drawing color
  float DRAWING_STRENGTH = 0.1;
  
  int W2,H2; 
  float xToW,yToH;
  float lastScreenX, lastScreenY;

  BackgroundBuffer backgroundBuffer;

  Particle(BackgroundBuffer bb, int _W2, int _H2, float _xToW, float _yToH) {
    backgroundBuffer = bb;
    W2 = _W2;
    H2 = _H2;
    xToW = _xToW;
    yToH = _yToH;
    draw_col = new GrafColor();
    resetLastScreenPos();
  }
  
  void resetLastScreenPos() {
    lastScreenX = -1;
    lastScreenY = -1;
  }

  void randomize() {  // randomize type, color and position
    type=(int)random(mainTypes); 
    typePerc=type/((float)mainTypes-1);
    damp=0.965+typePerc*0.015;
    draw_col.set(dotColVar[type]);
    x=random(-50,150); 
    y=random(-50,150);
    typeRel1=new float[nr_att];
    for (int i=0;i<nr_att;i++) {
      typeRel1[i]=(abs((type-att[i].type)/((float)mainTypes-1))-0.1)*att[i].speed;
    }
  }

  void updateDrawingColor() { 
    draw_col=dotColVar[type]; 
  }

  void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      if (d2!=0) {
        float typeRel=typeRel1[i]/d2;
        if (d2<500) {
          typeRel*=-1;
        }
        xm+=typeRel*dx; 
        ym+=typeRel*dy;
      }
    }
    xm*=damp; 
    ym*=damp;
    x+=xm; 
    y+=ym; 
  }

  void onScreen() { 
    float sx = W2+(x-50)*xToW;
    float sy = H2+(y-50)*yToH;
    float dx = lastScreenX - sx;
    float dy = lastScreenY - sy;
    float d = sqrt(dx*dx+dy*dy);
    if (d>1.4&&lastScreenX!=-1) {
      // draw line!
      float dxp = dx/d;
      float dyp = dy/d;
      float tsx = sx;
      float tsy = sy;
      float stren = DRAWING_STRENGTH*pow(1.0/d,1.5);
      for (int i=0;i<d;i++) {
        backgroundBuffer.setOnBg(tsx,tsy,draw_col,stren);
        tsx += dxp;
        tsy += dyp;
      }
    } else
      backgroundBuffer.setOnBg(sx,sy,draw_col,DRAWING_STRENGTH);   
    
    lastScreenX = sx;
    lastScreenY = sy;
  }
}
