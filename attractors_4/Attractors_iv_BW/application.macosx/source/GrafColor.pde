class GrafColor {
  
  public static final int MODE_8BIT  = 65536;
  public static final int MODE_16BIT = 65537;
  private int MODE = MODE_8BIT;
  
  int VAL_MAX = 0xFF;
  
  int R = 0;
  int G = 0;
  int B = 0;
  
  GrafColor() {
  }
  
  GrafColor(int mode) {
    changeMode(mode);
  }
  
  GrafColor(int r, int g, int b) {
    R = minMaxLim(r,0,VAL_MAX);
    G = minMaxLim(g,0,VAL_MAX);
    B = minMaxLim(b,0,VAL_MAX);
  }
  
  GrafColor(GrafColor gc) {
    set(gc);
  }
  
  private int minLim(int v, int v0) {
    return v<v0?v0:v;
  }
  
  private int maxLim(int v, int v1) {
    return v>v1?v1:v;
  }
  
  private int minMaxLim(int v, int v0, int v1) {
    return maxLim(minLim(v,v0),v1);
  }
  
  void set(GrafColor gc) {
    MODE = gc.MODE;
    VAL_MAX = gc.VAL_MAX;
    R = gc.R;
    G = gc.G;
    B = gc.B;
  }
  
  void changeMode(int mode) {
    if (mode==MODE_8BIT) {
      if (MODE==MODE_16BIT) {
        R = R>>8&0xFF;
        G = G>>8&0xFF;
        B = B>>8&0xFF;
      }
      MODE = MODE_8BIT;
      VAL_MAX = 0xFF;
    }
    else if (mode==MODE_16BIT) {
      if (MODE==MODE_8BIT) {
        R = R<<8;
        G = G<<8;
        B = B<<8;
      }
      MODE = MODE_16BIT;
      VAL_MAX = 0xFFFF;
    }
  }
  
  void mixWith(GrafColor gc2, float p2) {
    float p1 = 1.0 - p2;
    R = minMaxLim((int)(R*p1+gc2.R*p2),0,VAL_MAX);
    G = minMaxLim((int)(G*p1+gc2.G*p2),0,VAL_MAX);
    B = minMaxLim((int)(B*p1+gc2.B*p2),0,VAL_MAX);
  }
  
  void variateColor(int var) {
    if (MODE == MODE_16BIT) var = var<<8;
    int iVar = (int)random(-var,var);
    R = minMaxLim(R+iVar,0,VAL_MAX);
    G = minMaxLim(G+iVar,0,VAL_MAX);
    B = minMaxLim(B+iVar,0,VAL_MAX);
  }
    
  String toString() { 
    String msg = "R: "+R+" G:"+G+" B:"+B;
    if (MODE==MODE_8BIT) {
      msg+=" (8 bit)";
    } else {
      msg+=" (16 bit)";
    }
    return msg;
  }

  int as24BitInt() {
    if (MODE==MODE_8BIT) {
      return R<<16|B<<8|G;
    } else {
      return (R>>8&0xFF)<<16 | (G>>8&0xFF)<<8 | (B>>8&0xFF);
    }
  }
  
}
