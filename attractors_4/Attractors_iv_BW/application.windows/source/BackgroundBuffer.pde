class BackgroundBuffer {
  
  float FADE_TO_WHITE_STRENGTH = 0.03;//4;//02;
  int FRAMES_TO_DISSOLVE = 10;
  GrafColor WHITE;
  GrafColor[][] canvas;
  PImage img;
  int[][][] dissolveIndex;
  
  int WIDTH, HEIGHT;  
  GrafColor[][] bgBuf; 
  
  BackgroundBuffer(int w, int h) {
    WIDTH = w;
    HEIGHT = h;
    img = loadImage("canvas2_500x300.jpg");
    canvas = new GrafColor[WIDTH][HEIGHT];
    clear();
    WHITE = new GrafColor(0xFF,0xFF,0xFF);
    WHITE.changeMode(GrafColor.MODE_16BIT);
    
    dissolveIndex = new int[WIDTH][HEIGHT][2];
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) {
      dissolveIndex[x][y][0] = x;
      dissolveIndex[x][y][1] = y;
    }
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) {
      int tx = (int)random(WIDTH);
      int ty = (int)random(HEIGHT);
      int tempX = dissolveIndex[tx][ty][0];
      int tempY = dissolveIndex[tx][ty][1];
      dissolveIndex[tx][ty][0] = dissolveIndex[x][y][0];
      dissolveIndex[tx][ty][1] = dissolveIndex[x][y][1];
      dissolveIndex[x][y][0] = tempX;
      dissolveIndex[x][y][1] = tempY;
    }
  }
  
  void clear() { 
    bgBuf=new GrafColor[WIDTH][HEIGHT]; 
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) {
      int c = img.get(x,y);
      bgBuf[x][y] = new GrafColor(c>>16&0xFF,c>>8&0xFF,c&0xFF);
      bgBuf[x][y].changeMode(GrafColor.MODE_16BIT);
      canvas[x][y] = new GrafColor(bgBuf[x][y]);
    }
  }
  
  void setOnBg(float x, float y, GrafColor c, float p) {
    int x0 = (int)x, x1 = x0 + 1;
    int y0 = (int)y, y1 = y0 + 1;
    float px1 = x-x0, px0 = 1.0 - px1;
    float py1 = y-y0, py0 = 1.0 - py1;
    float p00 = p*px0*py0;
    float p01 = p*px0*py1;
    float p10 = p*px1*py0;
    float p11 = p*px1*py1;
    setOnBg(x0,y0,c,p00);
    setOnBg(x0,y1,c,p01);
    setOnBg(x1,y0,c,p10);
    setOnBg(x1,y1,c,p11);
  }
  
  void setOnBg(int x, int y, GrafColor c, float p) {
    if (x>=0&&x<WIDTH&&y>=0&&y<HEIGHT) {
      bgBuf[x][y].mixWith(c,p);
    }
  }
  
  void fadeToWhite() {
    
    int yMin = (int)(HEIGHT*(frameCount%FRAMES_TO_DISSOLVE)/(float)FRAMES_TO_DISSOLVE);
    int yMax = yMin + (int)(HEIGHT/(float)FRAMES_TO_DISSOLVE);
    for (int y=yMin;y<yMax;y++)
      for (int x=0;x<WIDTH;x++) {
        WHITE = canvas[dissolveIndex[x][y][0]][dissolveIndex[x][y][1]];
        bgBuf[dissolveIndex[x][y][0]][dissolveIndex[x][y][1]].mixWith(WHITE,FADE_TO_WHITE_STRENGTH);
      }
  }

  void drawbgBuf() { 
    for (int y=0;y<HEIGHT;y++)
      for (int x=0;x<WIDTH;x++)
        set(x,y,bgBuf[x][y].as24BitInt()); 
  }
  
}
