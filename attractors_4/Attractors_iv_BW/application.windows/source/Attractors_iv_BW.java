import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.*; 
import java.awt.image.*; 
import java.awt.event.*; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class Attractors_iv_BW extends PApplet {

/*
 Attractors IV v Black and White (16bit per channel color, anti-aliasing)
 created in 2005 by Marcus Graf
 marcus[AT]florito.net
 
 // click LEFT MOUSE BUTTON on screen to create new dots
 // press [i] or RIGHT MOUSE BUTTON to restart
 
 */

float particleDrawTime = 0, bufferFadeTime = 0, bufferDrawTime = 0;

int nr_att=16;
int max_nr_dot=6000;
int splotSize=15;
int mainTypes=12; 

Particle[] dot; 
int dotToSteal; 
int nr_dot=0;
GrafColor[] dotColVar;

float xToW,yToH;
boolean initializeAll=true;

BackgroundBuffer backgroundBuffer;

//PFont verdanaBold;
PFont verdanaItalic10;

//PImage arrow;
//String OUT_FILE = "D:\\Stroom Aanvraag\\DVD Stroom\\Sources\\Attractors_iv_BW\\frame";

public void setup() {
  size(500,300,P3D); 
  //verdanaBold = loadFont("Verdana-Bold-12.vlw");
  verdanaItalic10 = loadFont("Verdana-Italic-10.vlw");
  textFont(verdanaItalic10,10);
  backgroundBuffer = new BackgroundBuffer(width,height);
  xToW=1.2f*width/100.0f; 
  yToH=1.2f*width/100.0f; 
  
  /*
  arrow = loadImage("arrow.jpg");
  arrow.format = ARGB;
  PImage msk = loadImage("arrowMask.jpg");
  arrow.loadPixels();
  for (int i=0;i<arrow.pixels.length;i++) {
    int rgb = arrow.pixels[i]&0xFFFFFF;
    int a = msk.pixels[i]&0xFF;
    arrow.pixels[i]=a<<24|rgb;
  }
  arrow.updatePixels();
  */
}


public void draw() {
  //cursor(CROSS);
  int mx = mouseX, my=mouseY;
  if (initializeAll) {
    initializeAll=false;
    setupColors();
    setupAttractors();
    setupDots();
    backgroundBuffer.clear();
    nr_dot=0;
  }

  if (mousePressed&&mouseButton==LEFT) {  
    float dx = pmouseX-mouseX;
    float dy = pmouseY-mouseY;
    float d = sqrt(sq(dx)+sq(dy));
    if (d>1.4f) {
      for (float p=0;p<1.0f;p+=1/d)
        createNewSplot(mouseX+p*dx,mouseY+p*dy);
    } 
    else
      createNewSplot(mouseX,mouseY);
  }

  long ms = millis();
  backgroundBuffer.fadeToWhite();
  bufferFadeTime = 0.9f*bufferFadeTime + 0.1f*(millis()-ms);

  ms = millis();
  backgroundBuffer.drawbgBuf();
  bufferDrawTime = 0.9f*bufferDrawTime + 0.1f*(millis()-ms);

  updateAttractors();

  ms = millis();
  updateAndDrawDots();
  particleDrawTime = 0.9f*particleDrawTime + 0.1f*(millis()-ms);

  if (keyPressed&&key=='S') {
    //textFont(verdanaBold,12);
    //textAlign(LEFT);
    fill(128);
    println("backgroundBuffer draw time: "+nf(bufferDrawTime,2,1)+" ms");
    println("backgroundBuffer fade time: "+nf(bufferFadeTime,2,1)+" ms");
    println("particle draw time: "+nf(particleDrawTime,2,1)+" ms");
  }

  
  textAlign(RIGHT);
  /*
  if (mouseX>width-80&&mouseY>288&&!mousePressed) {
    cursor(12); 
    fill(128);
  } 
  else {
    cursor(0);
    fill(192);
  }
  */
  
  //text("www.florito.net",width-4,height-4);
  
  for (int x=0;x<width;x++) { set(x,0,0x333333); set(x,height-1,0x333333); }
  for (int x=1;x<width-1;x++) { set(x,1,0x999999); set(x,height-2,0x999999); }
//  for (int x=2;x<width-2;x++) { set(x,2,0xC0C0C0); set(x,height-3,0xc0c0c0); }
  for (int y=0;y<height;y++) { set(0,y,0x333333); set(width-1,y,0x333333); }
  for (int y=1;y<height-1;y++) { set(1,y,0x999999); set(width-2,y,0x999999); }
//  for (int y=2;y<height-2;y++) { set(2,y,0xC0C0C0); set(width-3,y,0xc0c0c0); }

  //image(arrow,mx,my); 
  //saveFrame(OUT_FILE+"_#####.tga");
}

public void keyPressed() { 
  if (key=='i') initializeAll=true;
}

public void mousePressed() {
  /*
  if (mouseButton==LEFT) {
    if (mouseX>width-80&&mouseY>288) {
      link("http://www.florito.net");//,"_blank");
    }
  }
  */
  if (mouseButton==RIGHT) initializeAll=true;
}




public void createNewSplot(float sx, float sy) {
  for (int i=0;i<splotSize;i++) {
    float rad = 0.5f * pow(random(1.0f),3);
    float rd = random(TWO_PI);
    dot[dotToSteal].x=50+(sx-width/2.0f)/xToW + rad*cos(rd); 
    dot[dotToSteal].y=50+(sy-height/2.f)/yToH + rad*sin(rd);
    dot[dotToSteal].xm=0;//0.5*random(-0.1,0.1); 
    dot[dotToSteal].ym=0;//0.5*random(-0.1,0.1);
    dot[dotToSteal].resetLastScreenPos();
    nr_dot++; 
    if (nr_dot>max_nr_dot) {
      nr_dot=max_nr_dot;
    }
    dotToSteal++; 
    if (dotToSteal==nr_dot) {
      dotToSteal=0;
    }
  }
}







// PARTICLES

public void setupDots() { 
  dot=new Particle[max_nr_dot]; 
  for (int i=0;i<max_nr_dot;i++) {
    dot[i]=new Particle(backgroundBuffer,width/2,height/2,xToW,yToH); 
    dot[i].randomize();
  } 
  dotToSteal=0; 
}
public void updateAndDrawDots() { 
  for (int i=0;i<nr_dot;i++) { 
    dot[i].update(); 
    dot[i].onScreen(); 
  } 
}









// ATTRACTORS

Attractor[] att;

public void setupAttractors() { 
  att=new Attractor[nr_att]; 
  for (int i=0;i<nr_att;i++) {
    att[i]=new Attractor(); 
    att[i].randomize();
  } 
}

public void updateAttractors() { 
  for (int i=0;i<nr_att;i++) { 
    att[i].update(); 
  } 
}

class Attractor {
  float x,y,xm,ym;
  int type;
  float frq,pha;
  float speed=1;
  Attractor() {
  }
  public void randomize() {
    type=(int)random(mainTypes);
    x=random(100); 
    y=random(100);
    float dir=atan2(50-y,50-x);
    if (random(1)<0.5f) {
      dir+=random(0.8f,1.2f)*HALF_PI;
    } 
    else {
      dir-=random(0.8f,1.2f)*HALF_PI;
    }
    xm=0.1f*cos(dir); 
    ym=0.1f*sin(dir);
  }
  public void update() {
    float dx=50-x, dy=50-y;
    float d2=sq(dx)+sq(dy);
    xm+=0.01f*(dx/d2); 
    ym+=0.01f*(dy/d2);
    x+=xm; 
    y+=ym;
  }
}





// COLOR FUNCTIONS:

public void setupColors() {
  dotColVar=new GrafColor[mainTypes]; 
  int br = (int)random(16,96);
  GrafColor dotCol=new GrafColor(br,br,br); 
  for (int i=0;i<mainTypes;i++) {
    dotColVar[i] = new GrafColor(dotCol);//dotCol);
    dotColVar[i].changeMode(GrafColor.MODE_16BIT);
    dotColVar[i].variateColor(60);
  }
}



class BackgroundBuffer {
  
  float FADE_TO_WHITE_STRENGTH = 0.03f;//4;//02;
  int FRAMES_TO_DISSOLVE = 10;
  GrafColor WHITE;
  GrafColor[][] canvas;
  PImage img;
  int[][][] dissolveIndex;
  
  int WIDTH, HEIGHT;  
  GrafColor[][] bgBuf; 
  
  BackgroundBuffer(int w, int h) {
    WIDTH = w;
    HEIGHT = h;
    img = loadImage("canvas2_500x300.jpg");
    canvas = new GrafColor[WIDTH][HEIGHT];
    clear();
    WHITE = new GrafColor(0xFF,0xFF,0xFF);
    WHITE.changeMode(GrafColor.MODE_16BIT);
    
    dissolveIndex = new int[WIDTH][HEIGHT][2];
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) {
      dissolveIndex[x][y][0] = x;
      dissolveIndex[x][y][1] = y;
    }
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) {
      int tx = (int)random(WIDTH);
      int ty = (int)random(HEIGHT);
      int tempX = dissolveIndex[tx][ty][0];
      int tempY = dissolveIndex[tx][ty][1];
      dissolveIndex[tx][ty][0] = dissolveIndex[x][y][0];
      dissolveIndex[tx][ty][1] = dissolveIndex[x][y][1];
      dissolveIndex[x][y][0] = tempX;
      dissolveIndex[x][y][1] = tempY;
    }
  }
  
  public void clear() { 
    bgBuf=new GrafColor[WIDTH][HEIGHT]; 
    for (int x=0;x<WIDTH;x++) for (int y=0;y<HEIGHT;y++) {
      int c = img.get(x,y);
      bgBuf[x][y] = new GrafColor(c>>16&0xFF,c>>8&0xFF,c&0xFF);
      bgBuf[x][y].changeMode(GrafColor.MODE_16BIT);
      canvas[x][y] = new GrafColor(bgBuf[x][y]);
    }
  }
  
  public void setOnBg(float x, float y, GrafColor c, float p) {
    int x0 = (int)x, x1 = x0 + 1;
    int y0 = (int)y, y1 = y0 + 1;
    float px1 = x-x0, px0 = 1.0f - px1;
    float py1 = y-y0, py0 = 1.0f - py1;
    float p00 = p*px0*py0;
    float p01 = p*px0*py1;
    float p10 = p*px1*py0;
    float p11 = p*px1*py1;
    setOnBg(x0,y0,c,p00);
    setOnBg(x0,y1,c,p01);
    setOnBg(x1,y0,c,p10);
    setOnBg(x1,y1,c,p11);
  }
  
  public void setOnBg(int x, int y, GrafColor c, float p) {
    if (x>=0&&x<WIDTH&&y>=0&&y<HEIGHT) {
      bgBuf[x][y].mixWith(c,p);
    }
  }
  
  public void fadeToWhite() {
    
    int yMin = (int)(HEIGHT*(frameCount%FRAMES_TO_DISSOLVE)/(float)FRAMES_TO_DISSOLVE);
    int yMax = yMin + (int)(HEIGHT/(float)FRAMES_TO_DISSOLVE);
    for (int y=yMin;y<yMax;y++)
      for (int x=0;x<WIDTH;x++) {
        WHITE = canvas[dissolveIndex[x][y][0]][dissolveIndex[x][y][1]];
        bgBuf[dissolveIndex[x][y][0]][dissolveIndex[x][y][1]].mixWith(WHITE,FADE_TO_WHITE_STRENGTH);
      }
  }

  public void drawbgBuf() { 
    for (int y=0;y<HEIGHT;y++)
      for (int x=0;x<WIDTH;x++)
        set(x,y,bgBuf[x][y].as24BitInt()); 
  }
  
}
class GrafColor {
  
  public static final int MODE_8BIT  = 65536;
  public static final int MODE_16BIT = 65537;
  private int MODE = MODE_8BIT;
  
  int VAL_MAX = 0xFF;
  
  int R = 0;
  int G = 0;
  int B = 0;
  
  GrafColor() {
  }
  
  GrafColor(int mode) {
    changeMode(mode);
  }
  
  GrafColor(int r, int g, int b) {
    R = minMaxLim(r,0,VAL_MAX);
    G = minMaxLim(g,0,VAL_MAX);
    B = minMaxLim(b,0,VAL_MAX);
  }
  
  GrafColor(GrafColor gc) {
    set(gc);
  }
  
  private int minLim(int v, int v0) {
    return v<v0?v0:v;
  }
  
  private int maxLim(int v, int v1) {
    return v>v1?v1:v;
  }
  
  private int minMaxLim(int v, int v0, int v1) {
    return maxLim(minLim(v,v0),v1);
  }
  
  public void set(GrafColor gc) {
    MODE = gc.MODE;
    VAL_MAX = gc.VAL_MAX;
    R = gc.R;
    G = gc.G;
    B = gc.B;
  }
  
  public void changeMode(int mode) {
    if (mode==MODE_8BIT) {
      if (MODE==MODE_16BIT) {
        R = R>>8&0xFF;
        G = G>>8&0xFF;
        B = B>>8&0xFF;
      }
      MODE = MODE_8BIT;
      VAL_MAX = 0xFF;
    }
    else if (mode==MODE_16BIT) {
      if (MODE==MODE_8BIT) {
        R = R<<8;
        G = G<<8;
        B = B<<8;
      }
      MODE = MODE_16BIT;
      VAL_MAX = 0xFFFF;
    }
  }
  
  public void mixWith(GrafColor gc2, float p2) {
    float p1 = 1.0f - p2;
    R = minMaxLim((int)(R*p1+gc2.R*p2),0,VAL_MAX);
    G = minMaxLim((int)(G*p1+gc2.G*p2),0,VAL_MAX);
    B = minMaxLim((int)(B*p1+gc2.B*p2),0,VAL_MAX);
  }
  
  public void variateColor(int var) {
    if (MODE == MODE_16BIT) var = var<<8;
    int iVar = (int)random(-var,var);
    R = minMaxLim(R+iVar,0,VAL_MAX);
    G = minMaxLim(G+iVar,0,VAL_MAX);
    B = minMaxLim(B+iVar,0,VAL_MAX);
  }
    
  public String toString() { 
    String msg = "R: "+R+" G:"+G+" B:"+B;
    if (MODE==MODE_8BIT) {
      msg+=" (8 bit)";
    } else {
      msg+=" (16 bit)";
    }
    return msg;
  }

  public int as24BitInt() {
    if (MODE==MODE_8BIT) {
      return R<<16|B<<8|G;
    } else {
      return (R>>8&0xFF)<<16 | (G>>8&0xFF)<<8 | (B>>8&0xFF);
    }
  }
  
}
class Particle {
  float x,y;           // screen position
  float xm=0, ym=0;    // move vector
  int type;            // type (defines how it reacts to attractor, and defines color)
  float typePerc;      // type, but rescaled to 0..1 (type/mainTypes);
  float damp;
  float[] typeRel1;    //
  
  GrafColor draw_col;  // drawing color
  float DRAWING_STRENGTH = 0.1f;
  
  int W2,H2; 
  float xToW,yToH;
  float lastScreenX, lastScreenY;

  BackgroundBuffer backgroundBuffer;

  Particle(BackgroundBuffer bb, int _W2, int _H2, float _xToW, float _yToH) {
    backgroundBuffer = bb;
    W2 = _W2;
    H2 = _H2;
    xToW = _xToW;
    yToH = _yToH;
    draw_col = new GrafColor();
    resetLastScreenPos();
  }
  
  public void resetLastScreenPos() {
    lastScreenX = -1;
    lastScreenY = -1;
  }

  public void randomize() {  // randomize type, color and position
    type=(int)random(mainTypes); 
    typePerc=type/((float)mainTypes-1);
    damp=0.965f+typePerc*0.015f;
    draw_col.set(dotColVar[type]);
    x=random(-50,150); 
    y=random(-50,150);
    typeRel1=new float[nr_att];
    for (int i=0;i<nr_att;i++) {
      typeRel1[i]=(abs((type-att[i].type)/((float)mainTypes-1))-0.1f)*att[i].speed;
    }
  }

  public void updateDrawingColor() { 
    draw_col=dotColVar[type]; 
  }

  public void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      if (d2!=0) {
        float typeRel=typeRel1[i]/d2;
        if (d2<500) {
          typeRel*=-1;
        }
        xm+=typeRel*dx; 
        ym+=typeRel*dy;
      }
    }
    xm*=damp; 
    ym*=damp;
    x+=xm; 
    y+=ym; 
  }

  public void onScreen() { 
    float sx = W2+(x-50)*xToW;
    float sy = H2+(y-50)*yToH;
    float dx = lastScreenX - sx;
    float dy = lastScreenY - sy;
    float d = sqrt(dx*dx+dy*dy);
    if (d>1.4f&&lastScreenX!=-1) {
      // draw line!
      float dxp = dx/d;
      float dyp = dy/d;
      float tsx = sx;
      float tsy = sy;
      float stren = DRAWING_STRENGTH*pow(1.0f/d,1.5f);
      for (int i=0;i<d;i++) {
        backgroundBuffer.setOnBg(tsx,tsy,draw_col,stren);
        tsx += dxp;
        tsy += dyp;
      }
    } else
      backgroundBuffer.setOnBg(sx,sy,draw_col,DRAWING_STRENGTH);   
    
    lastScreenX = sx;
    lastScreenY = sy;
  }
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#D4D0C8", "Attractors_iv_BW" });
  }
}
