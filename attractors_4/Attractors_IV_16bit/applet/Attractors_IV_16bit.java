import java.applet.*; import java.awt.*; import java.awt.image.*; import java.awt.event.*; import java.io.*; import java.net.*; import java.text.*; import java.util.*; import java.util.zip.*; import netscape.javascript.*; import javax.comm.*; import javax.sound.midi.*; import javax.sound.midi.spi.*; import javax.sound.sampled.*; import javax.sound.sampled.spi.*; import javax.xml.parsers.*; import javax.xml.transform.*; import javax.xml.transform.dom.*; import javax.xml.transform.sax.*; import javax.xml.transform.stream.*; import org.xml.sax.*; import org.xml.sax.ext.*; import org.xml.sax.helpers.*; public class Attractors_IV_16bit extends BApplet {// preset colors:
// preset[x]=new Col_16bit(5028,22876,27789);  //  nice blue
// preset[x]=new Col_16bit(8139,7963,21750);  //  purple blue dark ink
// preset[x]=new Col_16bit(14245,20093,687);  //  jungly green
// preset[x]=new Col_16bit(27931,12559,24505);  // nice wine red

int mainTypes; 
Attractor[] att; int nr_att;
Particle[] dot; int nr_dot, max_nr_dot; int dotToSteal;
Col_16bit dotCol; Col_16bit[] dotColVar;
Col_16bit[] bgBuf; 
int W,H,W2,H2; int maxpix; float xToW,yToH;
boolean newSplot;

void setup() {
  size(400,300); background(255); W=width; H=height; xToW=1.2f*W/100.0f; yToH=1.2f*H/100.0f; W2=W/2; H2=H/2; maxpix=W*H;
  
  mainTypes=12; 
  // setup color and color variations:
  dotColVar=new Col_16bit[mainTypes]; 
  dotCol=new Col_16bit((int)random(0,65536/2),(int)random(0,65536/2),(int)random(0,65536/2)); dotCol.toPrint();
  for (int i=0;i<mainTypes;i++) {dotColVar[i]=variateColor(dotCol,40*256);}
  
  // setup attractors and particles
  nr_att=16; att=new Attractor[nr_att]; for (int i=0;i<nr_att;i++) {att[i]=new Attractor(); att[i].randomize();}
  nr_dot=5000; max_nr_dot=5000; dot=new Particle[nr_dot]; for (int i=0;i<nr_dot;i++) {dot[i]=new Particle(); dot[i].randomize();} dotToSteal=0;
  
  // setup bgbuf;
  bgBuf=new Col_16bit[maxpix]; bgBuf=fillBuf(bgBuf,new Col_16bit(65535,65535,65535));

  newSplot=false; nr_dot=0;
}


void loop() {
  if (newSplot) {
    newSplot=false;
    for (int i=0;i<500;i++) {
      dot[dotToSteal].x=50+(mouseX-W2)/xToW; dot[dotToSteal].y=50+(mouseY-H2)/yToH;
      dot[dotToSteal].xm=random(-0.1f,0.1f); dot[dotToSteal].ym=random(-0.1f,0.1f);
      nr_dot++; if (nr_dot>max_nr_dot) {nr_dot=max_nr_dot;}
      dotToSteal++; if (dotToSteal==nr_dot) {dotToSteal=0;}
    }
    println("dots: "+nr_dot);
  }
  //drawing:
  bgBufFade(0.995f); 
  drawbgBuf();
  for (int i=0;i<nr_att;i++) {
    att[i].update(); 
  }
  
  for (int i=0;i<nr_dot;i++) {
    dot[i].update();
    dot[i].onScreen();
  }
}
void mousePressed() {
  newSplot=true;
}



class Particle {
  float x,y;           // screen position
  float xm=0, ym=0;    // move vector
  int type;            // type (defines how it reacts to attractor, and defines color)
  float typePerc;      // type, but rescaled to 0..1 (type/mainTypes);
  float[] typeRel1;    //
  Col_16bit draw_col;  // drawing color
  
  Particle() {
  }
  
  void randomize() {  // randomize type, color and position
    type=(int)random(mainTypes); typePerc=type/((float)mainTypes-1);
    draw_col=dotColVar[type];
    x=random(-50,150); y=random(-50,150);
    typeRel1=new float[nr_att];
    for (int i=0;i<nr_att;i++) {
      typeRel1[i]=(abs((type-att[i].type)/((float)mainTypes-1))-0.1f)*att[i].speed;
    }
  }
  
  void updateDrawingColor() {
    draw_col=dotColVar[type];
  }
  
  void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      if (d2!=0) {
        float typeRel=typeRel1[i]/d2;
        if (d2<500) {typeRel*=-1;}
        xm+=typeRel*dx; ym+=typeRel*dy;
      }
    }
    float damp=0.965f+typePerc*0.015f;
    xm*=damp; ym*=damp;
    x+=xm; y+=ym; 
  }
  
  void onScreen() {
//    setOnBg(int(x*xToW),int(y*yToH),draw_col);
    setOnBg((int)(W2+(x-50)*xToW),(int)(H2+(y-50)*yToH),draw_col);
  }
}



class Attractor {
  float x,y,xm,ym;
  int type;
  float frq,pha;
  float speed=1;
  Attractor() {
  }
  void randomize() {
    type=(int)random(mainTypes);
    x=random(100); y=random(100);
    float dir=atan2(50-y,50-x);
    if (random(1)<0.5f) {dir+=random(0.8f,1.2f)*HALF_PI;} else {dir-=random(0.8f,1.2f)*HALF_PI;}
    xm=0.1f*cos(dir); ym=0.1f*sin(dir);
  }
  void update() {
    float dx=50-x, dy=50-y;
    float d2=sq(dx)+sq(dy);
    xm+=0.01f*(dx/d2); ym+=0.01f*(dy/d2);
    x+=xm; y+=ym;
  }
}


class Position {
  float x,y;
  Position(float inx, float iny) {
    x=inx; y=iny;
  }
}

void setOnBg(int x, int y, Col_16bit c) {
  float p1=0.05f; float p2=1.0f-p1;
  if (x>=0&&x<W&&y>=0&&y<H) {
    int i=y*W+x;
    float r1=c.getRed(), g1=c.getGreen(), b1=c.getBlue();
    float r2=bgBuf[i].getRed(), g2=bgBuf[i].getGreen(), b2=bgBuf[i].getBlue();
    float rr=p1*r1+p2*r2, gg=p1*g1+p2*g2, bb=p1*b1+p2*b2;
    bgBuf[i]=new Col_16bit(rr,gg,bb);
  }
}

void bgBufFade(float val) {
  for (int i=0;i<maxpix;i++) {
    bgBuf[i]=fade_16bit(bgBuf[i],val);
  }
}

void drawbgBuf() {
  for (int x=0;x<W;x++) {
    for (int y=0;y<H;y++) {
      int i=y*W+x;
      set(x,y,bgBuf[i].toNormRGB());
    }
  }
}

Col_16bit[] fillBuf(Col_16bit[] buf,Col_16bit c) {
  buf=bgBuf;
  for (int i=0;i<bgBuf.length;i++) {
    buf[i]=c;
  }
  return buf;
}




class Col_16bit {
  float rr,gg,bb;
  Col_16bit(float r, float g, float b) {
    rr=r; gg=g; bb=b;
  }
  float getRed() {
    return rr;
  }
  float getGreen() {
    return gg;
  }
  float getBlue() {
    return bb;
  }
  int toNormRGB() {
    int r8=(int)(constrain(rr/256.0f,0,255));
    int g8=(int)(constrain(gg/256.0f,0,255));
    int b8=(int)(constrain(bb/256.0f,0,255));
    return (r8<<16|g8<<8|b8);
  }
  void toPrint() {
    println("preset[x]=new Col_16bit("+(int)(rr)+","+(int)(gg)+","+(int)(bb)+");  // ");
  }
}

Col_16bit[] variateColors(Col_16bit ttc,int imax) {
  Col_16bit[] outcol=new Col_16bit[imax];
  for (int i=0;i<imax;i++) {
    outcol[i]=variateColor(ttc,255*50);
  }
  return outcol;
}

Col_16bit variateColor(Col_16bit tc,float coff) {
  float rr=constrain(tc.getRed()+random(-coff,coff),0,65535);
  float gg=constrain(tc.getGreen()+random(-coff,coff),0,65535);
  float bb=constrain(tc.getBlue()+random(-coff,coff),0,65535);
  return (new Col_16bit(rr,gg,bb));
}

Col_16bit fade_16bit(Col_16bit inCol,float perc) {  // fade to white 0.999 is slow, 0.001 is super fast
  float rr=65535-inCol.getRed();
  float gg=65535-inCol.getGreen();
  float bb=65535-inCol.getBlue();
  rr=65535-rr*perc;
  gg=65535-gg*perc;
  bb=65535-bb*perc;
  return (new Col_16bit(rr,gg,bb));
}
}