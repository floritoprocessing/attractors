// preset colors:
// preset[x]=new Col_16bit(5028,22876,27789);  //  nice blue
// preset[x]=new Col_16bit(8139,7963,21750);  //  purple blue dark ink
// preset[x]=new Col_16bit(14245,20093,687);  //  jungly green
// preset[x]=new Col_16bit(27931,12559,24505);  // nice wine red

int W,H,W2,H2; 
int maxpix; float xToW,yToH;
boolean newSplot, restart;

void setup() {
  size(1024,768,P3D);  
  W=width; H=height; xToW=1.2*W/100.0; yToH=1.2*H/100.0; W2=W/2; H2=H/2; maxpix=W*H;
  
  newSplot=false; restart=true; 
}


void draw() {
  if (restart) {
    restart=false;
    background(255);
    setupColors();
    initAttractors();
    initParticles();
    nr_dot=0;
  }
  
  if (newSplot) {
    newSplot=false;
    for (int i=0;i<500;i++) {
      dot[dotToSteal].x=50+(mouseX-W2)/xToW; dot[dotToSteal].y=50+(mouseY-H2)/yToH;
      dot[dotToSteal].xm=random(-0.1,0.1); dot[dotToSteal].ym=random(-0.1,0.1);
      nr_dot++; if (nr_dot>max_nr_dot) {nr_dot=max_nr_dot;}
      dotToSteal++; if (dotToSteal==nr_dot) {dotToSteal=0;}
    }
    println("dots: "+nr_dot);
  }
  
  updateAttractors();
  updateParticles();

  loadPixels();
  fadeOutAll();
  
  
}

void mousePressed() { newSplot=true; }
//void mouseReleased() { newSplot=false; }
void keyPressed() { if (key==' ') {restart=true;}}


//////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// COLOUR SETUP ////////////////////////////////////////

int mainTypes=12; 
colNorm dotCol; 
colNorm[] dotColVar;

void setupColors() {
  dotColVar=new colNorm[mainTypes]; 
  dotCol=new colNorm((int)random(0,256/2),(int)random(0,256/2),(int)random(0,256/2)); dotCol.toPrint();
  for (int i=0;i<mainTypes;i++) {dotColVar[i]=variateColor(dotCol,40);}
}

//////////////////////////////////////// end COLOUR SETUP ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////////





////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// PARTICLE CLASS ////////////////////////////////////////

Particle[] dot; 
int nr_dot, max_nr_dot; 
int dotToSteal;

void initParticles() {
  nr_dot=5000; max_nr_dot=5000; dot=new Particle[nr_dot]; 
  for (int i=0;i<nr_dot;i++) {dot[i]=new Particle(); dot[i].randomize();} 
  dotToSteal=0;
}

void updateParticles() {
  for (int i=0;i<nr_dot;i++) {
    dot[i].update();
    dot[i].onScreen();
  }
}

class Particle {
  float x,y;           // screen position
  float xm=0, ym=0;    // move vector
  int type;            // type (defines how it reacts to attractor, and defines color)
  float typePerc;      // type, but rescaled to 0..1 (type/mainTypes);
  float[] typeRel1;    //
  colNorm draw_col;  // drawing color
  
  Particle() {
  }
  
  void randomize() {  // randomize type, color and position
    type=(int)random(mainTypes); typePerc=type/((float)mainTypes-1);
    draw_col=dotColVar[type];
    x=random(-50,150); y=random(-50,150);
    typeRel1=new float[nr_att];
    for (int i=0;i<nr_att;i++) {
      typeRel1[i]=(abs((type-att[i].type)/((float)mainTypes-1))-0.1)*att[i].speed;
    }
  }
  
  void updateDrawingColor() {
    draw_col=dotColVar[type];
  }
  
  void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      if (d2!=0) {
        float typeRel=typeRel1[i]/d2;
        if (d2<500) {typeRel*=-1;}
        xm+=typeRel*dx; ym+=typeRel*dy;
      }
    }
    float damp=0.965+typePerc*0.015;
    xm*=damp; ym*=damp;
    x+=xm; y+=ym; 
  }
  
  void onScreen() {
    setSoft(int(W2+(x-50)*xToW),int(H2+(y-50)*yToH),draw_col);
  }
}

//////////////////////////////////////// end PARTICLE CLASS ////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////





/////////////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// ATTRACTOR CLASS ////////////////////////////////////////

Attractor[] att; int nr_att;

void initAttractors() {
  nr_att=16; att=new Attractor[nr_att]; 
  for (int i=0;i<nr_att;i++) {att[i]=new Attractor(); att[i].randomize();}
}

void updateAttractors() {
  for (int i=0;i<nr_att;i++) { att[i].update(); }
}

class Attractor {
  float x,y,xm,ym;
  int type;
  float frq,pha;
  float speed=1;
  Attractor() {
  }
  void randomize() {
    type=(int)random(mainTypes);
    x=random(100); y=random(100);
    float dir=atan2(50-y,50-x);
    if (random(1)<0.5) {dir+=random(0.8,1.2)*HALF_PI;} else {dir-=random(0.8,1.2)*HALF_PI;}
    xm=0.1*cos(dir); ym=0.1*sin(dir);
  }
  void update() {
    float dx=50-x, dy=50-y;
    float d2=sq(dx)+sq(dy);
    xm+=0.01*(dx/d2); ym+=0.01*(dy/d2);
    x+=xm; y+=ym;
  }
}

//////////////////////////////////////// end ATTRACTOR CLASS ////////////////////////////////////////
/////////////////////////////////////////////////////////////////////////////////////////////////////





//////////////////////////////////////////////////////////////////////////////////////////
//////////////////////////////////////// FADE OUT ////////////////////////////////////////

int fc=0;
void fadeOutAll() {
  fc++;
  if (fc==25) {
    fc=0;
    for (int i=0;i<maxpix;i++) { int pc=pixels[i]; if (pc!=0) { pixels[i]=fadeOut(pc,0.99); } }
  }
}

//////////////////////////////////////// end FADE OUT ////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////////////////////////





class colNorm {
  int rr,gg,bb;
  colNorm(int r, int g, int b) {
    rr=r; gg=g; bb=b;
  }
  int getRed() { return rr; }
  int getGreen() { return gg; }
  int getBlue() { return bb; }
  int toNormRGB() { return (rr<<16|gg<<8|bb); }
  void toPrint() {
    println("preset[x]=new colNorm("+int(rr)+","+int(gg)+","+int(bb)+");  // ");
  }
}

colNorm[] variateColors(colNorm ttc,int imax) {
  colNorm[] outcol=new colNorm[imax];
  for (int i=0;i<imax;i++) {
    outcol[i]=variateColor(ttc,50);
  }
  return outcol;
}

colNorm variateColor(colNorm tc,float coff) {
  int rr=(int)constrain(tc.getRed()+random(-coff,coff),0,255);
  int gg=(int)constrain(tc.getGreen()+random(-coff,coff),0,255);
  int bb=(int)constrain(tc.getBlue()+random(-coff,coff),0,255);
  return (new colNorm(rr,gg,bb));
}

int fadeOut(int inCol, float perc) {
  int rr=255-(inCol>>16&255);
  int gg=255-(inCol>>8&255);
  int bb=255-(inCol&255);
  rr=255-int(rr*perc);
  gg=255-int(gg*perc);
  bb=255-int(bb*perc);
  return (rr<<16|gg<<8|bb);
}

void setSoft(int x, int y, colNorm c) {
  float p1=0.05; float p2=1.0-p1;
  if (x>=0&&x<W&&y>=0&&y<H) {
    int i=y*W+x;
    float r1=c.getRed(), g1=c.getGreen(), b1=c.getBlue();
    float r2=red(pixels[i]), g2=green(pixels[i]), b2=blue(pixels[i]);
    int rr=int(p1*r1+p2*r2), gg=int(p1*g1+p2*g2), bb=int(p1*b1+p2*b2);
    pixels[i]=(rr<<16|gg<<8|bb);
  }
}
