Attractor[] att; int nr_att;
Particle[] dot; int nr_dot;
int mainTypes; int dotCol;
int[] bgBuf; int maxpix;
int W,H;
int keyInit;

void setup() {
  size(600,450);background(255); W=width/2; H=height/2; maxpix=W*H; 
  mainTypes=6;
  initMainColor();
  nr_att=50; initAttractors();
  nr_dot=5000; initParticles();
  bgBuf=new int[maxpix]; bgBuf=fillBuf(bgBuf,256*256*256-1);
  keyInit=0;
}

void initMainColor() {
  dotCol=(int)random(256)<<16|(int)random(256)<<8|(int)random(256);
}
void initAttractors() {
  att=new Attractor[nr_att]; for (int i=0;i<nr_att;i++) {att[i]=new Attractor(); att[i].randomize();}
}
void initParticles() {
  dot=new Particle[nr_dot]; for (int i=0;i<nr_dot;i++) {dot[i]=new Particle(dotCol); dot[i].randomize();}
}


void keyPressed() {
  if (key=='a') {keyInit=2;}
  if (key=='p') {keyInit=3;}
}

void loop() {
  if (keyPressed&&key=='f') {bgBufFade();}
  if (keyInit!=0) {
    if (keyInit==2) {initAttractors();}    
    if (keyInit==3) {initParticles();}
    keyInit=0;
  }
  
  background(255);
  //bgBufFade();
  drawbgBuf();
  for (int i=0;i<nr_att;i++) {
    att[i].update(); //att[i].onScreen();
  }
  for (int i=0;i<nr_dot;i++) {
    dot[i].update();
    dot[i].onScreen();
  }
  copyFourTimes();
}

class Particle {
  float x,y;
  float xm=0, ym=0;
  int type;
  int c;
  int[] col;
  
  Particle(int tc) {
    col=new int[mainTypes];
    col=variateColors(tc,mainTypes);
  }
  
  void randomize() {
    type=(int)random(mainTypes);
    c=col[type];
    x=random(W); y=random(H);
  }
  void update() {
    for (int i=0;i<nr_att;i++) {
      float dx=att[i].x-x, dy=att[i].y-y;
      float d2=sq(dx)+sq(dy);
      float typeRel=abs(type-att[i].type)/((float)mainTypes-1)-0.5;
      if (d2>0.2) { xm+=typeRel*att[i].speed*dx/d2; ym+=typeRel*att[i].speed*dy/d2; }
    }
    xm*=0.9; ym*=0.9;
    x+=xm; y+=ym; 
//    if (x>width-1) {x-=width;} if (x<=0) {x+=width;}
//    if (y>height-1) {y-=height;} if (y<=0) {y+=height;}
  }
  
  void onScreen() {
//    set(int(x),int(y),c);
    setBgAndS(int(x),int(y),c);
  }
}

int[] variateColors(int ttc,int imax) {
  int[] outcol=new int[imax];
  for (int i=0;i<imax;i++) {
    outcol[i]=variateColor(ttc,50);
  }
  return outcol;
}
  



class Attractor {
  float x,y;
  int type;
  float frq,pha;
  float spd=4, speed;
  Attractor() {
  }
  void randomize() {
    type=(int)random(mainTypes);
    frq=random(0.05,0.30); pha=random(TWO_PI);
    x=random(W); y=random(H);
  }
  void update() {
    speed=spd;//*sin(frq*millis()/1000.0+pha);//
  }
  void onScreen() {
    noStroke();
    fill(255,0,0); ellipseMode(CENTER_DIAMETER); ellipse(x,y,10,10);
  }
}

void setBgAndS(int x, int y, int c) {
  //set(x,y,c);
  float p1=0.1; float p2=1.0-p1;
  if (x>=0&&x<W&&y>=0&&y<H) {
    int i=y*W+x;
    int r1=c>>16&255, g1=c>>8&255, b1=c&255;
    int r2=bgBuf[i]>>16&255, g2=bgBuf[i]>>8&255, b2=bgBuf[i]&255;
    int rr=int(p1*r1+p2*r2), gg=int(p1*g1+p2*g2), bb=int(p1*b1+p2*b2);
    bgBuf[i]=rr<<16|gg<<8|bb;
  }
}

void bgBufFade() {
  for (int i=0;i<maxpix;i++) {
    int rr=bgBuf[i]>>16&255;
    int gg=bgBuf[i]>>8&255;
    int bb=bgBuf[i]&255;
    if (rr<255) {rr++;}
    if (gg<255) {gg++;}
    if (bb<255) {bb++;}
    bgBuf[i]=rr<<16|gg<<8|bb;
  }
}

void drawbgBuf() {
  for (int x=0;x<W;x++) {
    for (int y=0;y<H;y++) {
      int i=y*W+x;
      set(x,y,bgBuf[i]);
    }
  }
}

int[] fillBuf(int[] buf,int c) {
  buf=bgBuf;
  for (int i=0;i<bgBuf.length;i++) {
    buf[i]=c;
  }
  return buf;
}

void copyFourTimes() {
  for (int x=0;x<W;x++) {
    for (int y=0;y<H;y++) {
      int c=get(x,y);
      int x2=width-x-1, y2=height-y-1;
      set(x2,y,c); set(x,y2,c); set(x2,y2,c);
    }
  }
}



int variateColor(int tc,int coff) {
  int rr=int(constrain((tc>>16&255)+random(-coff,coff),0,255));
  int gg=int(constrain((tc>>8&255)+random(-coff,coff),0,255));
  int bb=int(constrain((tc&255)+random(-coff,coff),0,255));
  return rr<<16|gg<<8|bb;
}
