class Particle extends VecParticle {
  
  QUtil qu = new QUtil();
  QColor col = new QColor(0xFFFF,0x3600,0x1500);
  double minGravDistance = 12.0;
  
  Particle(double x, double y) {
    super(x,y);
    qu.setBlendMode(QUtil.BLEND_MODE_ADD);
  }
  
  void updateMov(Vector atts) {
    acc.set(0,0,0);
    double minGravDist2=minGravDistance*minGravDistance;
    
    for (int i=0;i<atts.size();i++) {
      Attractor a = (Attractor)atts.elementAt(i);
      Vec d = vecSub(a.getPos(),pos);
      double dx = d.getX();
      double dy = d.getY();
      double d2 = d.lenSQ();
      if (d2<minGravDist2) d2=minGravDist2;
      double F = 5.0/d2;
      acc.add(new Vec(dx*F,dy*F));
    }
    
    mov.add(acc);
    mov.mul(0.9995);
  }
  
  void resetIfFarAway(double x0, double y0, double x1, double y1, int w, int h) {
    if (pos.getX()<x0) pos.set(random(w),random(h));
    if (pos.getX()>x1) pos.set(random(w),random(h));
    if (pos.getY()<y0) pos.set(random(w),random(h));
    if (pos.getY()>y1) pos.set(random(w),random(h));
  }
  
  void draw(QImage img) {
    qu.set(img,(float)pos.getX(),(float)pos.getY(),col,0.01);
  }
}
