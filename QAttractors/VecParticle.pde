class VecParticle {
  
  Vec pos = new Vec();
  Vec mov = new Vec();
  Vec acc = new Vec();
  
  VecParticle() {
  }
  
  VecParticle(double x, double y) {
    pos.set(x,y);
  }
  
  VecParticle(double x, double y, double z) {
    pos.set(x,y,z);
  }
  
  Vec getPos() {
    return pos;
  }
  
  public void move() {
    pos.add(mov);
  }
  
}
