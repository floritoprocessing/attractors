class Attractor extends VecParticle {
  
  QColor col = new QColor(0x3000,0x7200,0xFFFF);
  QUtil qu = new QUtil();
  
  double ROTATION_SPEED_MIN   = 0.05;
  double ROTATION_SPEED_RANGE = 0.07;
  double RADIUS_MIN = 50.0;
  double RADIUS_MAX = 70.0;
  double PHASE_ADD_MIN = 0.000001;
  double PHASE_ADD_MAX = 0.000003;
  double rotSpeed, rd, radius;
  double phaseX, phaseY;
  double phaseXAdd, phaseYAdd;
  
  Vec origPos;
  
  Vec[] lastPos;
  
  Attractor(double x, double y) {
    super(x,y);
    origPos = new Vec(pos);
    
    rotSpeed = 2*Math.PI*(ROTATION_SPEED_MIN+ROTATION_SPEED_RANGE*Math.random());
    phaseX = 2*Math.PI*Math.random();
    phaseY = 2*Math.PI*Math.random();
    phaseXAdd = 2*Math.PI*(PHASE_ADD_MIN+(PHASE_ADD_MAX-PHASE_ADD_MIN))*Math.random();
    phaseYAdd = 2*Math.PI*(PHASE_ADD_MIN+(PHASE_ADD_MAX-PHASE_ADD_MIN))*Math.random();
    radius = RADIUS_MIN + (RADIUS_MAX-RADIUS_MIN)*Math.random();
    rd = 0;
    
    lastPos = new Vec[40];
    for (int i=0;i<lastPos.length;i++) lastPos[i] = new Vec(origPos);
  }
  
  void updatePosition() {
    phaseX += phaseXAdd;
    phaseY += phaseYAdd;
    rd += rotSpeed;
    if (rd>TWO_PI) rd-=TWO_PI;
    if (rd<-TWO_PI) rd+=TWO_PI;
    
    Vec offVec = new Vec(radius*Math.cos(rd+phaseX),radius*Math.sin(rd+phaseY));
    pos.set(vecAdd(origPos,offVec));
    
    for (int i=0;i<lastPos.length-1;i++) lastPos[i].set(lastPos[i+1]);
    lastPos[lastPos.length-1].set(pos);
  }
  
  void draw(QImage img) {
    qu.circle(img, (float)pos.getX(), (float)pos.getY(), 5, col, 1.0);
  }
  
  void show() {
    noFill();
    ellipseMode(RADIUS);
    for (int i=0;i<lastPos.length;i++) {
      stroke(0,0,255,32+64*(i/(float)lastPos.length));
      ellipse((float)lastPos[i].getX(), (float)lastPos[i].getY(), 5 , 5);
    }
  }
}
