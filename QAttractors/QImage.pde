/*
*
*  class QImage is similar to PImage, only that it uses QColors which are 16 bit per channel
*
*  CONSTRUCTOR SUMMARY:
*  --------------------
*
*  QImage (int width, int height)
*    Constructs a new QImage with the specified inital width and height.
*    Sets all pixels to black. (QColor(0,0,0))
*
*  FIELD SUMMARY:
*  --------------
*
*  int     width
*            returns the QImage width in pixels
*  int     height
*            returns the QImage height in pixels
*
*  METHOD SUMMARY:
*  ---------------
*
*  void    background(QColor qcolor)
*            sets the entire QImage to qcolor
*
*  void    background(int red, int green, int blue)
*            sets the entire QImage to the QColor with the specified channel amounts
*
*  void    set(int x, int y, Qcolor c)
*            sets the specified pixel at x/y to color c
*
*  QColor  get(int x, int y)
*            returns the color at the specified pixel x/y. if x/y is out of boundaries, will return BLACK
*
*  PImage  asPImage()
*            updates the visual representation of the QImage if necessary and returns it
*
*/

class QImage {
  
  public int width, height;
  
  private QColor[][] img;
  private PImage pimage;
  private boolean PImage_updated;
  
  QImage(int w, int h) {
    width = w;
    height = h;
    img = new QColor[w][h];
    pimage = new PImage(w,h);
    for (int x=0;x<w;x++) for (int y=0;y<h;y++) {
      img[x][y] = new QColor();
      pimage.set(x,y,color(0,0,0));
    }
    PImage_updated = true;
  }
  
  public void background(QColor c) {
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) {
      img[x][y].set(c);
      pimage.set(x,y,c.asColor());
    }
    PImage_updated = true;
  }
  
  public void background(int r, int g, int b) {
    QColor c = new QColor(r,g,b);
    background(c);
  }
  
  public void set(int x, int y, QColor c) {
    if (x<0) return;
    if (x>=width) return;
    if (y<0) return;
    if (y>=height) return;
    img[x][y].set(c);
    PImage_updated = false;
  }
  
  public QColor get(int x, int y) {
    if (x<0) return new QColor();
    if (x>=width) return new QColor();
    if (y<0) return new QColor();
    if (y>=height) return new QColor();
    return img[x][y];
  }
  
  private void updatePImage() {
    for (int x=0;x<width;x++) for (int y=0;y<height;y++) pimage.set(x,y,img[x][y].asColor());
    PImage_updated = true;
  }
  
  public PImage asPImage() {
    if (!PImage_updated) updatePImage();
    return pimage;
  }
  
}
