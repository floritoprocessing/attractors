/*
*
*  class QUtil is a utility class for color blending, point/line drawing etc of QImages and QColors
*
*  CONSTRUCTOR SUMMARY:
*  --------------------
*
*  QUtil()
*    Constructs a new QUtil object 
*
*  FIELD SUMMARY:
*  --------------
*
*  static int     BLEND_MODE_NORMAL
*                   use with setBlendMode(int blendMode) to change drawing mode
*  static int     BLEND_MODE_ADD
*                   use with setBlendMode(int blendMode) to change drawing mode
*  static int     BLEND_MODE_SUBTRACT
*                   use with setBlendMode(int blendMode) to change drawing mode
*
*  METHOD SUMMARY:
*  ---------------
*  void    setBlendMode(int blendMode)
*            sets the blending mode used with this QUtil object.
*            i.e. setBlendMode(QUtil.BLEND_MODE_ADD)
*            
*  QColor  colorBlend(QColor color1, QColor color2, double opacity1)
*            returns the result of blending color1 and color2 with percentage1 (0..1) using the actual blending mode
*
*  void    set(QImage image, int x, int y, QColor color, float opacity)
*            sets a pixel on image at x/y to the color with specified opacity using the actual blending mode
*
*  void    set(QImage image, float x, float y, QColor color, float opacity)
*            sets an antialiased pixel on image at x/y to the color with specified opacity using the actual blending mode
*
*  void    line(QImage image, int x0, int y0, int x1, int y1, QColor color, float opacity)
*            draws a line from point x0/y0 to point x1/y1 on to the specified image with color and opacity using the actual blending mode
*
*  void    line(QImage image, float x0, float y0, float x1, float y1, QColor color, float opacity)
*            draws an antialiased line from point x0/y0 to point x1/y1 on to the specified image with color and opacity using the actual blending mode
*
*/

class QUtil {

  public static final int BLEND_MODE_NORMAL = 0;
  public static final int BLEND_MODE_ADD = 1;
  public static final int BLEND_MODE_SUBTRACT = 2;
  private int BLEND_MODE = BLEND_MODE_NORMAL;
  
  QUtil() {
  }

  public void setBlendMode(int m) {
    BLEND_MODE = m;
  }

  public QColor colorBlend(QColor c0, QColor c1, double p0) {
    int r=0, g=0, b=0;

    if (BLEND_MODE==BLEND_MODE_NORMAL) {
      double p1 = 1.0 - p0;
      r = (int)(c0.red()*p0+c1.red()*p1);  
      g = (int)(c0.green()*p0+c1.green()*p1);
      b = (int)(c0.blue()*p0+c1.blue()*p1);
    } else if (BLEND_MODE==BLEND_MODE_ADD) {
      r = (int)(c0.red()*p0+c1.red());  
      g = (int)(c0.green()*p0+c1.green());
      b = (int)(c0.blue()*p0+c1.blue());
    } else if (BLEND_MODE==BLEND_MODE_SUBTRACT) {
      r = (int)(c1.red()-c0.red()*p0);
      g = (int)(c1.green()-c0.green()*p0);
      b = (int)(c1.blue()-c0.blue()*p0);
    }
    return new QColor(r,g,b);
  }

  public void set(QImage img, int x, int y, QColor c, float p) {
    QColor outColor = new QColor();
    outColor = colorBlend(c,img.get(x,y),p);
    img.set(x,y,outColor);
  }

  public void set(QImage img, float x, float y, QColor c, float p) {
    int x0 = int(floor(x)), x1 = x0 + 1;
    int y0 = int(floor(y)), y1 = y0 + 1;
    float px1 = x - x0, px0 = 1.0 - px1;
    float py1 = y - y0, py0 = 1.0 - py1;
    float p00 = p * px0 * py0, p10 = p * px1* py0;
    float p01 = p * px0 * py1, p11 = p * px1* py1;
    set(img,x0,y0,c,p00);
    set(img,x1,y0,c,p10);
    set(img,x0,y1,c,p01);
    set(img,x1,y1,c,p11);
  }

  public void line(QImage img, int x0, int y0, int x1, int y1, QColor c, float p) {
    int dx=x1-x0, dy=y1-y0;
    float d=sqrt(dx*dx+dy*dy);
    if (d!=0) {
      for (int i=0;i<=d;i++) {
        float pl = i/(float)d;
        set(img,int(x0+pl*dx),int(y0+pl*dy),c,p);
      }
    } 
    else {
      set(img,x0,y0,c,p);
    }
  }

  public void line(QImage img, float x0, float y0, float x1, float y1, QColor c, float p) {
    float dx=x1-x0, dy=y1-y0;
    float d=sqrt(dx*dx+dy*dy);
    if (d!=0) {
      for (int i=0;i<=d;i++) {
        float pl = i/(float)d;
        set(img,x0+pl*dx,y0+pl*dy,c,p);
      }
    } 
    else {
      set(img,x0,y0,c,p);
    }
  }
  
  public void circle(QImage img, float x, float y, float r, QColor c, float p) {
    float U = TWO_PI*r;
    for (float rd=0;rd<TWO_PI;rd+=TWO_PI/U) {
      set(img,x+r*cos(rd),y+r*sin(rd),c,p);
    }
  }

}
