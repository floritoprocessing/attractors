// QAttractors
import java.util.Vector;

QColor COLOR_BACKGROUND = new QColor(0,0,0);

Vector attractors;
Vector particles;
QImage img;
QUtil qu;

boolean attractorsVisible = true;

void setup() {
  size(400,300,P3D);

  attractors = new Vector();
  for (int i=0;i<5;i++) attractors.add(new Attractor(random(0.2*width,0.8*width),random(0.2*height,0.8*height)));

  particles = new Vector();
  for (int i=0;i<10000;i++) particles.add(new Particle(random(width),random(height)));

  img = new QImage(width,height);
  img.background(COLOR_BACKGROUND);
  
  qu = new QUtil();
}

void fadeBackground() {
  float FADE=0.001;
  for (int x=0;x<img.width;x++) for (int y=0;y<img.height;y++) {
    qu.set(img,x,y,COLOR_BACKGROUND,FADE);
  }
}

void keyPressed() {
  if (key=='a') attractorsVisible = !attractorsVisible;
}

void draw() {
  
  fadeBackground();
  
  for (int i=0;i<attractors.size();i++) ((Attractor)attractors.elementAt(i)).updatePosition();

  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).updateMov(attractors);
  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).move();
  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).resetIfFarAway(-2*width,-2*height,3*width,3*height,width,height);

  //for (int i=0;i<attractors.size();i++) ((Attractor)attractors.elementAt(i)).draw(img);
  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).draw(img);

  image(img.asPImage(),0,0);
  
  if (attractorsVisible) {
    for (int i=0;i<attractors.size();i++) ((Attractor)attractors.elementAt(i)).show();
  }
  
  
}
