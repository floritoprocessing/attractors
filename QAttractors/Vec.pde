/*****************************
 *  Vec class and functions  *
 *****************************/

class Vec {
  private double x=0,y=0,z=0;

  Vec() {
  }
  
  Vec(double _x, double _y) {
    x=_x;
    y=_y;
  }
  
  Vec(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }

  Vec(Vec _v) {
    x=_v.getX(); 
    y=_v.getY(); 
    z=_v.getZ();
  }

  void set(Vec _v) {
    x=_v.x; 
    y=_v.y; 
    z=_v.z;
  }

  void set(double _x, double _y) {
    x=_x;
    y=_y;
  }
  
  void set(double _x, double _y, double _z) {
    x=_x;
    y=_y;
    z=_z;
  }
  
  void setX(double v) { x=v; }
  void setY(double v) { y=v; }
  void setZ(double v) { z=v; }
  
  double getX() { return x; }
  double getY() { return y; }
  double getZ() { return z; }

  double len() {
    return Math.sqrt(x*x+y*y+z*z);
  }

  double lenSQ() {
    return (x*x+y*y+z*z);
  }

  void gravitateTo(Vec _to, double _mgd, double _fFac) {
    double minGravDist2=_mgd*_mgd;
    double dx=_to.x-x;
    double dy=_to.y-y;
    double dz=_to.z-z;
    double d2=dx*dx+dy*dy+dz*dz;
    if (d2<minGravDist2) {
      d2=minGravDist2;
    }
    //double d=Math.sqrt(d2);
    double F=_fFac/d2;
    add(new Vec(dx*F,dy*F,dz*F));
  }

  void add(Vec _v) {
    x+=_v.x;
    y+=_v.y;
    z+=_v.z;
  }

  void add(double _x, double _y, double _z) {
    x+=_x;
    y+=_y;
    z+=_z;
  }

  void sub(Vec _v) {
    x-=_v.x;
    y-=_v.y;
    z-=_v.z;
  }

  void mul(double p) {
    x*=p;
    y*=p;
    z*=p;
  }

  void div(double p) {
    x/=p;
    y/=p;
    z/=p;
  }

  void negX() {
    x=-x;
  }

  void negY() {
    y=-y;
  }

  void negZ() {
    z=-z;
  }

  void neg() {
    negX();
    negY();
    negZ();
  }

  void normalize() {
    double l=len();
    if (l!=0) { // if not nullvector
      div(l);
    } 
    else {
      x=0;
      y=0;
      z=0;
    }
  }

  Vec getNormalized() {
    double l=len();
    if (l!=0) {
      Vec out=new Vec(this);
      out.div(l);
      return out;
    } 
    else {
      return new Vec();
    }
  }

  void setLen(double ml) {
    //    Vec out=new Vec(this);
    double l=len();
    if (l!=0) {
      double fac=ml/l;
      x*=fac;
      y*=fac;
      z*=fac;
    }
  }

  void toAvLen(double ml) {
    Vec out=new Vec(this);
    double fac=ml/((ml+len())/2.0);
    x*=fac;
    y*=fac;
    z*=fac;
  }

  void rotX(double rd) {
    double SIN=Math.sin(rd); 
    double COS=Math.cos(rd);
    double yn=y*COS-z*SIN;
    double zn=z*COS+y*SIN;
    y=yn;
    z=zn;
  }

  void rotY(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-z*SIN; 
    double zn=z*COS+x*SIN;
    x=xn;
    z=zn;
  }

  void rotZ(double rd) {
    double SIN=Math.sin(rd);
    double COS=Math.cos(rd); 
    double xn=x*COS-y*SIN; 
    double yn=y*COS+x*SIN;
    x=xn;
    y=yn;
  }

  void rot(double xrd, double yrd, double zrd) {
    rotX(xrd);
    rotY(yrd);
    rotZ(zrd);
  }

  boolean isNullVec() {
    if (x==0&&y==0&&z==0) {
      return true;
    } 
    else {
      return false;
    }
  }

  void clip(Vec c) {
    while (x<0) x+=c.x;
    while (x>=c.x) x-=c.x;
    while (y<0) y+=c.y;
    while (y>=c.y) y-=c.y;
    while (z<0) z+=c.z;
    while (z>=c.z) z-=c.z;
  }

  void constrain(Vec mi, Vec ma) {
    if (x<mi.x) x=mi.x;
    if (x>ma.x) x=ma.x;
    if (y<mi.y) y=mi.y;
    if (y>ma.y) y=ma.y;
    if (z<mi.z) z=mi.z;
    if (z>ma.z) z=ma.z;
  }

  double getLongitude() {
    return Math.atan2(z,x);
  }

  double getLatitude() {
    Vec tmp=new Vec(this);
    tmp.rotY(-getLongitude());
    return Math.atan2(tmp.y,tmp.x);
  }

  double[] getLonLat() {
    double[] lonlat=new double[2];
    lonlat[0]=Math.atan2(z,x);
    Vec tmp=new Vec(this);
    tmp.rotY(-lonlat[0]);
    lonlat[1]=Math.atan2(tmp.y,tmp.x);
    return lonlat;
  }

  String toString() {
    return (x+"\t"+y+"\t"+z);
  }
}




Vec vecAdd(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.add(b);
  return out;
}

Vec vecSub(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.sub(b);
  return out;
}

Vec vecMul(Vec a, double b) {
  Vec out=new Vec(a);
  out.mul(b);
  return out;
}

Vec vecComponentMul(Vec a, Vec b) {
  Vec out=new Vec(a);
  out.x*=b.x;
  out.y*=b.y;
  out.z*=b.z;
  return out;
}

double vecMulSkalar(Vec a, Vec b) {
  double out=a.x*b.x+a.y*b.y+a.z*b.z;
  return out;
}

Vec vecDiv(Vec a, double b) {
  Vec out=new Vec(a);
  out.div(b);
  return out;
}

double vecLen(Vec a) {
  return a.len();
}

Vec vecRotY(Vec a, double rd) {
  Vec out=new Vec(a);
  out.rotY(rd);
  return out;
}

Vec rndVec() {
  return new Vec(random(-1,1),random(-1,1),random(-1,1));
}

Vec rndDirVec(double r) {
  Vec out=new Vec(0,0,r);
  out.rotX(random(-PI/2.0,PI));
  out.rotZ(random(0,2*PI));
  return out;
}

Vec rndDirVecXY(double r) {
  Vec out=new Vec(r,0,0);
  out.rotZ(random(0,2*PI));
  return out;
}

Vec rndPlusMinVec(double s) {
  Vec out=new Vec(-s+2*s*Math.random(),-s+2*s*Math.random(),-s+2*s*Math.random());
  return out;
}

void vecSwap(Vec v1, Vec v2) {
  Vec t=new Vec(v1);
  v1.set(v2);
  v2.set(t);
}

double vecDistance(Vec v1, Vec v2) {
  return Math.sqrt(sq((float)(v2.x-v1.x)+(float)(v2.y-v1.y)+(float)(v2.z-v1.z)));
}

double vecAngleBetween(Vec v1, Vec v2) {
  double out=Math.acos(vecMulSkalar(v1,v2)/(vecLen(v1)*vecLen(v2)));
  return out;
}


/*****************************
 * adapted 3d functions to Vec
 *****************************/

void vecTranslate(Vec v) {
  translate((float)v.x,(float)v.y,(float)v.z);
}

void vecVertex(Vec v) {
  vertex((float)v.x,(float)v.y,(float)v.z);
}

void vecLine(Vec a, Vec b) {
  line((float)a.x,(float)a.y,(float)a.z,(float)b.x,(float)b.y,(float)b.z);
}

void vecRect(Vec a, Vec b) {
  rect((float)a.x,(float)a.y,(float)b.x,(float)b.y);
}

void vecCamera(Vec eye, Vec cen, Vec upaxis) {
  camera((float)eye.x,(float)eye.y,(float)eye.z,(float)cen.x,(float)cen.y,(float)cen.z,(float)upaxis.x,(float)upaxis.y,(float)upaxis.z);
}
