/*
  RUNNING UNDER PROCESSING 0098
  
  A set of Particles move across the screen and leave traces.
  They have a random speed and direction.
  When they encounter Attractors, they get targeted to the attractors direction.
  
  Press 's' to show the attractors.
  Press 'p' to pause/continue the application.
  Press 'f' to show frame number.
  
*/

int nrOfParticles=10000;

// PRESET TWO:
int nrOfAttractors=60;
float attractorMin=0.01, attractorMax=0.1;
boolean colorGravity=true;
int wid=1600, hei=800;

// PRESET ONE:
// int nrOfAttractors=10;
// float attractorMin=0.05, attractorMax=0.3;
// boolean colorGravity=false;
// int wid=500, hei=1000;

// main color ranges
int colRLo=0, colRHi=128;
int colGLo=20, colGHi=100;
int colBLo=0, colBHi=25;
float maxColorDiff;

boolean showAttractors=true;
boolean paused=false;





void setup() {
  size(1600,800); colorMode(RGB,255);
  maxColorDiff=getColorDifference(color(colRLo,colGLo,colBLo),color(colRHi,colGHi,colBHi));
  initAttractors(nrOfAttractors,width,height);
  initParticles(nrOfParticles,width,height);
  background(255);
  loadPixels();
  println("[s] to toggle showing attractors");
  println("[p] to toggle pause");
  println("[f] to print frame number");
  println("[S] to save frame");
}

void draw() {
  if (!paused) {
    background(255);
    updatePixels();
    updateParticlePosition(nrOfParticles);
    drawParticles(nrOfParticles,0.3);
    loadPixels();
    if (showAttractors) {drawAttractors(nrOfAttractors);}
  }
  if (keyPressed && key=='S') {
    println("Saving frame "+frameCount);
    saveFrame("Attractor_X_#####.png");
  }
}

void keyPressed() { 
  if (key=='s') {showAttractors=!showAttractors;} 
  if (key=='p') {paused=!paused; println("Paused: "+paused);}
  if (key=='f') {println("Frame number: "+frameCount);}
}






Attractor[] attractor=new Attractor[nrOfAttractors];

void initAttractors(int _n,int _w, int _h) { for (int i=0;i<_n;i++) { attractor[i]=new Attractor(i,_w,_h); } }
void drawAttractors(int _n) { for (int i=0;i<_n;i++) { attractor[i].drawMe(); } }

class Attractor {
  int id=0;
  float sScale=400;
  float x=0,y=0;
  float xMax=0,yMax=0;
  float radius=0;
  float turn=0;
  float dir=0;
  int gravityDirection=1;
  color col=makeRandomMainColor();
  
  Attractor(int _id, int _w,int _h) {
    id=_id;
    sScale=_w>_h?_w:_h;
    if (_w>_h) { xMax=1.0; yMax=_h/(float)_w; } else { yMax=1.0; xMax=_w/(float)_h; }
    radius=attractorMin+sq(random(1))*(attractorMax-attractorMin);  // more small ones
    turn=radians(random(0,2)); if (random(1)<0.5) {turn=-turn;}
    dir=random(-PI,PI);
    gravityDirection=random(1)<0.5?-1:1;
    
    // position circles without too much overlapping
    boolean found=false;
    while (!found) {
      x=random(xMax); y=random(yMax);
      boolean overlap=false;
      for (int i=0;i<id;i++) {
        float d=dist(x,y,attractor[i].x,attractor[i].y);
        float maxR=radius>attractor[i].radius?radius:attractor[i].radius;
        if (d<maxR) {overlap=true;}
      }
      if (!overlap) {found=true;}
    }
  }
  
  void drawMe() {
    strokeWeight(10);
    stroke(col); noFill(); ellipseMode(CENTER);
    ellipse(int(x*sScale),int(y*sScale),radius*sScale,radius*sScale);
  }
}







// begin
// LIBRARY PARTICLE
// HOLDS:
//
// -  class Particle(width,height)
//    position: x,m    position limit: xMax, yMax  [0..1]
//    movement: xm,ym
//    bounce: bouncyness in respect to border (true/false)
//    col: color of particle
// -  array particle with nrOfParticle items
// -  void initParticlePosition(arraylength,width,height)
// -  void updateParticles(arraylength)
// -  void drawParticles(arraylength)

Particle[] particle=new Particle[nrOfParticles];

void initParticles(int _n,int _w, int _h) { for (int i=0;i<_n;i++) { particle[i]=new Particle(_w,_h); } }
void updateParticlePosition(int _n) { for (int i=0;i<_n;i++) { particle[i].update(); } }
void drawParticles(int _n, float _p) { for (int i=0;i<_n;i++) { particle[i].drawMe(_p); } }

class Particle {
  float sScale;                          // scaleFac from 0..xMax/yMax to screensize
  float x=0, y=0;                        // x/y position
  float xMax=1, yMax=1;                  // x/y maximum value (minimum is always 0)
  float xm=0, ym=0;                      // x/y movement vector
  float r=0,rd=0;                        // movement vector in radians and length
  float[] distanceToAttractor=new float[nrOfAttractors]; // distance to all attractors
  float intensity=0, targetIntensity=0;  // drawing intensity -> influences color and draw 'pressure'
  color baseCol=makeRandomMainColor();   // drawing colour
  color col=baseCol;
  float[] colDiffToAtt=new float[nrOfAttractors]; // difference between colour and attractor colour [0..1]
  
  // INIT PARTICLE
  Particle(int _w,int _h) {
    // find xMax and yMax; longer side is always length 1.0
    sScale=_w>_h?_w:_h;
    if (_w>_h) { xMax=1.0; yMax=_h/(float)_w; } else { yMax=1.0; xMax=_w/(float)_h; }
    // set position and initial movement vector:
    x=random(xMax); y=random(yMax);
    xm=random(0.0005,0.001); ym=random(0.0005,0.001);
    if (random(1)<0.5) {xm=-xm;} if (random(1)<0.5) {ym=-ym;}
    // set movement vector in components strength and direction:
    r=sqrt(sq(xm)+sq(ym)); rd=atan2(ym,xm);
    // set array colour differences between particle and attractors
    for (int a=0;a<nrOfAttractors;a++) { 
      colDiffToAtt[a]=1.0-getColorDifference(col,attractor[a].col)/maxColorDiff; // normalized [0..1] colour difference between particle and attractor 
    }
  }
  
  // UPDATE POSITION, MOVEMENT OF PARTICLE
  void update() {
    // update direction and intensity if within attractor
    boolean withinOneAttractor=false;
    for (int a=0;a<nrOfAttractors;a++) {
      distanceToAttractor[a]=dist(x,y,attractor[a].x,attractor[a].y);
      // if within attractor: 
      if (distanceToAttractor[a]<attractor[a].radius) {
        // change direction according to attractor dir:
        withinOneAttractor=true; 
//        float dToTurn=getTurnDirection(rd+colDiffToAtt[a]*TWO_PI,attractor[a].dir); // change according to attractor dir and color difference
        float dToTurn=getTurnDirection(rd,attractor[a].dir);
        rd+=0.01*dToTurn;
        // change intensity if within attractor:
        targetIntensity+=(colDiffToAtt[a]-0.5)*0.01;
      }
    }
    if (!withinOneAttractor) {targetIntensity-=0.01;}
    targetIntensity=constrain(targetIntensity,0,1);
    intensity=(40*intensity+targetIntensity)/41.0;

    // update colour depending on intensity value
    col=mix(baseCol,color(255,255,255),sqrt(intensity));
        
    // update xm/ym-vectors if within an attractor (if change direction)
    if (withinOneAttractor) { xm=r*cos(rd); ym=r*sin(rd); }
    
    // gravity only if outside 2xradius attractor and less than 20% of screen distance
    for (int a=0;a<nrOfAttractors;a++) {
      if (distanceToAttractor[a]>2*attractor[a].radius&&distanceToAttractor[a]<0.2) {
        float dx=attractor[a].x-x, dy=attractor[a].y-y;
        float s=0.0000001*attractor[a].gravityDirection/sq(distanceToAttractor[a]);
        if (colorGravity) {s*=colDiffToAtt[a];}
        xm+=s*dx; ym+=s*dy;
      }
    }
    r=sqrt(sq(xm)+sq(ym)); rd=atan2(ym,xm);
    
    // MOVE PARTICLE TO NEW POSITION
    x+=xm; y+=ym; 
    if (x<0) {x+=xMax;} if (x>=xMax) {x-=xMax;}
    if (y<0) {y+=yMax;} if (y>=yMax) {y-=yMax;}
  }
  
  // DRAW THE PARTICLE
  void drawMe(float _p) {
    sSet(int(x*sScale),int(y*sScale),col,intensity*_p);
  }
}

// LIBRARY PARTICLE 
// end





// calculate shortest degree from deg1 to deg2
float getTurnDirection(float deg1, float deg2) {
  while (deg1>PI) {deg1-=TWO_PI;} while (deg2<-PI) {deg1+=TWO_PI;}
  while (deg2>PI) {deg2-=TWO_PI;} while (deg2<-PI) {deg2+=TWO_PI;}
  
  float rdeg=(deg1>deg2)?deg2-(deg1-TWO_PI):deg2-deg1;
  if (rdeg<PI) { return rdeg; } else { return (rdeg-TWO_PI); }
}

// drawing routines
void sSet(float _x, float _y, int c, float p) {
  int x=int(_x); int y=int(_y);
  int bg=get(x,y);
  set(x,y,mix(c,bg,p));
}

color mix(int a, int b, float p) {
  float q=1.0-p;
  int rr=int(ch_red(a)*p+ch_red(b)*q);
  int gg=int(ch_grn(a)*p+ch_grn(b)*q);
  int bb=int(ch_blu(a)*p+ch_blu(b)*q);
  return color(rr,gg,bb);
}

int ch_red(int c) { return (c>>16&255); }
int ch_grn(int c) { return (c>>8&255); }
int ch_blu(int c) { return (c&255); }

float getColorDifference(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float rd=abs(r1-r2)/765.0, gd=abs(g1-g2)/765.0, bd=abs(b1-b2)/765.0;
  return (rd+gd+bd);
}

color makeRandomMainColor() {
  return color(random(colRLo,colRHi),random(colGLo,colGHi),random(colBLo,colBHi));
}
