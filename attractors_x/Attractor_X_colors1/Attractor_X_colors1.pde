/*
  RUNNING UNDER PROCESSING 0098
  
  A set of Particles move across the screen and leave traces.
  They have a random speed and direction.
  When they encounter Attractors, they get targeted to the attractors direction.
  
  Press 's' to show the attractors.
  Press 'p' to pause/continue the application.
  Press 'f' to show frame number.
  
*/



int nrOfParticles=10000;

// PRESET ONE:
// see "preset1.jpg"
// int nrOfAttractors=10;
// float attRadMin=0.05, attRadMax=0.3;
// float prtSpdMin=0.0005, prtSpdMax=0.001;
// float drawingStrength=0.3;
// boolean gColorGravity=false;
// boolean gTurnToColor=false;
// int wid=500, hei=1000;

// PRESET TWO:
// see "preset2.jpg"
// int nrOfAttractors=60;
// float attRadMin=0.01, attRadMax=0.1;
// float prtSpdMin=0.0009, prtSpdMax=0.0011;
// float drawingStrength=0.3;
// boolean gColorGravity=true;
// boolean gTurnToColor=false;
// int wid=1600, hei=800;

// PRESET 3

 int nrOfAttractors=80;
 float attRadMin=0.01, attRadMax=0.05;
 float prtSpdMin=0.0005, prtSpdMax=0.001;
 float drawingStrength=1.0;
 boolean gColorGravity=true;
 boolean gTurnToColor=true;
 int wid=500, hei=400;


// main color ranges
int colRLo=0, colRHi=128;
int colGLo=20, colGHi=100;
int colBLo=0, colBHi=25;
float maxColorDiff;

boolean showAttractors=true;
boolean paused=false;





void setup() {
  size(500,400); colorMode(RGB,255);
  maxColorDiff=getColorDifference(color(colRLo,colGLo,colBLo),color(colRHi,colGHi,colBHi));
  initAttractors(nrOfAttractors,width,height,attRadMin,attRadMax);
  initParticles(nrOfParticles,width,height,prtSpdMin,prtSpdMax);
  background(255);
  loadPixels();
}

void draw() {
  if (!paused) {
    background(255);
    updatePixels();
    updateParticlePosition(nrOfParticles);
    drawParticles(nrOfParticles,drawingStrength);
    loadPixels();
    if (showAttractors) {drawAttractors(nrOfAttractors);}
  }
}

void keyPressed() { 
  if (key=='s'||key=='S') {showAttractors=!showAttractors;} 
  if (key=='p'||key=='P') {paused=!paused; println("Paused: "+paused);}
  if (key=='f'||key=='F') {println("Frame number: "+frameCount);}
}






Attractor[] attractor=new Attractor[nrOfAttractors];

void initAttractors(int _n,int _w, int _h, float _radMin, float _radMax) { 
  // _n:                number of Attractors
  // _w, _h:            screen width/height
  // _radMin, _radMax:  Attractor radius minimum/maximum
  for (int i=0;i<_n;i++) { attractor[i]=new Attractor(i,_w,_h,_radMin,_radMax); } 
}
void drawAttractors(int _n) { for (int i=0;i<_n;i++) { attractor[i].drawMe(); } }

class Attractor {
  int id=0;
  float sScale=400;
  float x=0,y=0;
  float xMax=0,yMax=0;
  float radius=0;
  float turn=0;
  float dir=0;
  int gravityDirection=1;
  color col=makeRandomMainColor();
  
  Attractor(int _id, int _w,int _h, float _radMin, float _radMax) {
    id=_id;
    sScale=_w>_h?_w:_h;
    if (_w>_h) { xMax=1.0; yMax=_h/(float)_w; } else { yMax=1.0; xMax=_w/(float)_h; }
    radius=_radMin+sq(random(1))*(_radMax-_radMin);  // more small ones
    turn=radians(random(0,2)); if (random(1)<0.5) {turn=-turn;}
    dir=random(-PI,PI);
    gravityDirection=random(1)<0.5?-1:1;
    
    // position circles without too much overlapping
    boolean found=false;
    while (!found) {
      x=random(xMax); y=random(yMax);
      boolean overlap=false;
      for (int i=0;i<id;i++) {
        float d=dist(x,y,attractor[i].x,attractor[i].y);
        float maxR=radius>attractor[i].radius?radius:attractor[i].radius;
        if (d<maxR) {overlap=true;}
      }
      if (!overlap) {found=true;}
    }
  }
  
  void drawMe() {
    strokeWeight(10);
    stroke(col); noFill(); ellipseMode(CENTER);
    ellipse(int(x*sScale),int(y*sScale),radius*sScale,radius*sScale);
  }
}







// begin
// LIBRARY PARTICLE
// HOLDS:
//
// -  class Particle(width,height)
//    position: x,m    position limit: xMax, yMax  [0..1]
//    movement: xm,ym
//    bounce: bouncyness in respect to border (true/false)
//    col: color of particle
// -  array particle with nrOfParticle items
// -  void initParticlePosition(arraylength,width,height)
// -  void updateParticles(arraylength)
// -  void drawParticles(arraylength)

Particle[] particle=new Particle[nrOfParticles];

void initParticles(int _n,int _w, int _h,float _mMin, float _mMax) { for (int i=0;i<_n;i++) { particle[i]=new Particle(_w,_h,_mMin,_mMax); } }
void updateParticlePosition(int _n) { for (int i=0;i<_n;i++) { particle[i].update(); } }
void drawParticles(int _n, float _p) { for (int i=0;i<_n;i++) { particle[i].drawMe(_p); } }

class Particle {
  float sScale;                          // scaleFac from 0..xMax/yMax to screensize
  float x=0, y=0;                        // x/y position
  float xMax=1, yMax=1;                  // x/y maximum value (minimum is always 0)
  float xm=0, ym=0;                      // x/y movement vector
  float r=0,rd=0;                        // movement vector in radians and length
  float[] distanceToAttractor=new float[nrOfAttractors]; // distance to all attractors
  float intensity=0, targetIntensity=0;  // drawing intensity -> influences color and draw 'pressure'
  color baseCol=makeRandomMainColor();   // drawing colour
  color col=baseCol;
  float[] colDiffToAtt=new float[nrOfAttractors]; // difference between colour and attractor colour [0..1]
  
  // INIT PARTICLE
  Particle(int _w,int _h, float _mMin, float _mMax) {
    // find xMax and yMax; longer side is always length 1.0
    sScale=_w>_h?_w:_h;
    if (_w>_h) { xMax=1.0; yMax=_h/(float)_w; } else { yMax=1.0; xMax=_w/(float)_h; }
    // set position and initial movement vector:
    x=random(xMax); y=random(yMax);
    xm=random(_mMin,_mMax); ym=random(_mMin,_mMax);
    if (random(1)<0.5) {xm=-xm;} if (random(1)<0.5) {ym=-ym;}
    // set movement vector in components strength and direction:
    r=sqrt(sq(xm)+sq(ym)); rd=atan2(ym,xm);
    // set array colour differences between particle and attractors
    for (int a=0;a<nrOfAttractors;a++) { 
      colDiffToAtt[a]=1.0-getColorDifference(col,attractor[a].col)/maxColorDiff; // normalized [0..1] colour difference between particle and attractor 
    }
  }
  
  // UPDATE POSITION, MOVEMENT OF PARTICLE
  void update() {
    // update direction and intensity if within attractor
    boolean withinOneAttractor=false;
    for (int a=0;a<nrOfAttractors;a++) {
      distanceToAttractor[a]=dist(x,y,attractor[a].x,attractor[a].y);
      // if within attractor: 
      if (distanceToAttractor[a]<attractor[a].radius) {
        // change direction according to attractor dir:
        withinOneAttractor=true; 
        
        float dToTurn;
        if (gTurnToColor) {
          dToTurn=getTurnDirection(rd+colDiffToAtt[a]*TWO_PI/maxColorDiff,attractor[a].dir); // change according to attractor dir and color difference
        } else {
          dToTurn=getTurnDirection(rd,attractor[a].dir);
        }
        rd+=0.01*dToTurn;
        // change intensity if within attractor:
        targetIntensity+=(colDiffToAtt[a]-0.5)*0.01;
      }
    }
    if (!withinOneAttractor) {targetIntensity-=0.01;}
    targetIntensity=constrain(targetIntensity,0,1);
    intensity=(40*intensity+targetIntensity)/41.0;

    // update colour depending on intensity value
    col=mixColor(baseCol,color(255,255,255),sqrt(intensity));
        
    // update xm/ym-vectors if within an attractor (if change direction)
    if (withinOneAttractor) { xm=r*cos(rd); ym=r*sin(rd); }
    
    // gravity only if outside 2xradius attractor and less than 20% of screen distance
    for (int a=0;a<nrOfAttractors;a++) {
      if (distanceToAttractor[a]>2*attractor[a].radius&&distanceToAttractor[a]<0.2) {
        float dx=attractor[a].x-x, dy=attractor[a].y-y;
        float s=0.0000001*attractor[a].gravityDirection/sq(distanceToAttractor[a]);
        if (gColorGravity) {s*=colDiffToAtt[a];}
        xm+=s*dx; ym+=s*dy;
      }
    }
    r=sqrt(sq(xm)+sq(ym)); rd=atan2(ym,xm);
    
    // MOVE PARTICLE TO NEW POSITION
    x+=xm; y+=ym; 
    if (x<0) {x+=xMax;} if (x>=xMax) {x-=xMax;}
    if (y<0) {y+=yMax;} if (y>=yMax) {y-=yMax;}
  }
  
  // DRAW THE PARTICLE
  void drawMe(float _p) {
    sSetAntialias(x*sScale,y*sScale,col,intensity*_p);
  }
}

// LIBRARY PARTICLE 
// end





// calculate shortest degree from deg1 to deg2
float getTurnDirection(float deg1, float deg2) {
  while (deg1>PI) {deg1-=TWO_PI;} while (deg2<-PI) {deg1+=TWO_PI;}
  while (deg2>PI) {deg2-=TWO_PI;} while (deg2<-PI) {deg2+=TWO_PI;}
  float rdeg=(deg1>deg2)?deg2-(deg1-TWO_PI):deg2-deg1;
  if (rdeg<PI) { return rdeg; } else { return (rdeg-TWO_PI); }
}

float getColorDifference(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float rd=abs(r1-r2)/765.0, gd=abs(g1-g2)/765.0, bd=abs(b1-b2)/765.0;
  return (rd+gd+bd);
}

color makeRandomMainColor() {
  return color(random(colRLo,colRHi),random(colGLo,colGHi),random(colBLo,colBHi));
}


void sSetAntialias(float _x, float _y, color _c, float _p) {
  int[] aapx=new int[4];
  int[] aapy=new int[4];
  float[] aapd=new float[4];
  
  //antialias pixel x/y positions:
  aapx[0]=int(floor(_x)); aapy[0]=int(floor(_y));
  aapx[1]=aapx[0]+1; aapy[1]=aapy[0];
  aapx[2]=aapx[0]; aapy[2]=aapy[0]+1;
  aapx[3]=aapx[0]+1; aapy[3]=aapy[0]+1;
  
  float xPartLeft=1.0-(_x-aapx[0]);
  float yPartTop=1.0-(_y-aapy[0]);
  
  aapd[0]=xPartLeft*yPartTop;
  aapd[1]=(1.0-xPartLeft)*yPartTop;
  aapd[2]=xPartLeft*(1.0-yPartTop);
  aapd[3]=(1.0-xPartLeft)*(1.0-yPartTop);
  
  for (int i=0;i<4;i++) {
    int px=int(aapx[i]), py=int(aapy[i]);
    set(px%width,py%height,mixColor(_c,get(px%width,py%height),_p*aapd[i]));
  }
}

color mixColor(color c1, color c2, float p) {
  float q=1.0-p;
  return color(p*red(c1)+q*red(c2),p*green(c1)+q*green(c2),p*blue(c1)+q*blue(c2));
}
