/*

 RUNNING UNDER PROCESSING 0098

 A set of Particles move across the screen and leave traces.
 They have a random speed and direction.
 When they encounter Attractors, they get targeted to the attractors direction.
 
 Press 's' to show the attractors.
 Press 'p' to pause/continue the application.
 Press 'f' to show frame number.
 
 
 
 */



int nrOfParticles=10000;

// PRESET ONE:
// see "preset1.jpg"
int nrOfAttractors=10;
float attRadMin=0.05, attRadMax=0.3;
float prtSpdMin=0.0005, prtSpdMax=0.001;
float drawingStrength=0.3;
boolean gColorGravity=false;
boolean gTurnToColor=false;
int wid=500, hei=1000;

// PRESET TWO:

// see "preset2.jpg"
/*
 int nrOfAttractors=60;
 float attRadMin=0.01, attRadMax=0.1;
 float prtSpdMin=0.0009, prtSpdMax=0.0011;
 float drawingStrength=0.3;
 boolean gColorGravity=true;
 boolean gTurnToColor=false;
 int wid=1600, hei=800;
 */

// PRESET 3
/*
 int nrOfAttractors=80;
 float attRadMin=0.01, attRadMax=0.05;
 float prtSpdMin=0.0005, prtSpdMax=0.001;
 float drawingStrength=1.0;
 boolean gColorGravity=true;
 boolean gTurnToColor=true;
 int wid=500, hei=400;
 */

// main color ranges
int colRLo=0, colRHi=128;
int colGLo=20, colGHi=100;
int colBLo=0, colBHi=25;
float maxColorDiff;

boolean showAttractors=true;
boolean paused=false;



Attractor[] attractor=new Attractor[nrOfAttractors];
Particle[] particle=new Particle[nrOfParticles];

void setup() {
  size(500,1000); 
  colorMode(RGB,255);
  maxColorDiff=getColorDifference(color(colRLo,colGLo,colBLo),color(colRHi,colGHi,colBHi));
  initAttractors(nrOfAttractors,width,height,attRadMin,attRadMax);
  initParticles(nrOfParticles,width,height,prtSpdMin,prtSpdMax);
  background(255);
  loadPixels();
}

void draw() {
  if (!paused) {
    background(255);
    updatePixels();
    updateParticlePosition(nrOfParticles);
    drawParticles(nrOfParticles,drawingStrength);
    loadPixels();
    if (showAttractors) {
      drawAttractors(nrOfAttractors);
    }
  }
  if (keyPressed && key=='S') {
    println("saving frame "+frameCount);
    saveFrame("Attractor_X_color2_#####.png");
  }
}

void keyPressed() { 
  if (key=='s') {
    showAttractors=!showAttractors;
  } 
  if (key=='p') {
    paused=!paused; 
    println("Paused: "+paused);
  }
  if (key=='f') {
    println("Frame number: "+frameCount);
  }
}














// begin
// LIBRARY PARTICLE
// HOLDS:
//
// -  class Particle(width,height)
//    position: x,m    position limit: xMax, yMax  [0..1]
//    movement: xm,ym
//    bounce: bouncyness in respect to border (true/false)
//    col: color of particle
// -  array particle with nrOfParticle items
// -  void initParticlePosition(arraylength,width,height)
// -  void updateParticles(arraylength)
// -  void drawParticles(arraylength)



// LIBRARY PARTICLE 
// end





// calculate shortest degree from deg1 to deg2
float getTurnDirection(float deg1, float deg2) {
  while (deg1>PI) {
    deg1-=TWO_PI;
  } 
  while (deg2<-PI) {
    deg1+=TWO_PI;
  }
  while (deg2>PI) {
    deg2-=TWO_PI;
  } 
  while (deg2<-PI) {
    deg2+=TWO_PI;
  }
  float rdeg=(deg1>deg2)?deg2-(deg1-TWO_PI):deg2-deg1;
  if (rdeg<PI) { 
    return rdeg; 
  } 
  else { 
    return (rdeg-TWO_PI); 
  }
}

float getColorDifference(color a, color b) {
  float r1=red(a), g1=green(a), b1=blue(a);
  float r2=red(b), g2=green(b), b2=blue(b);
  float rd=abs(r1-r2)/765.0, gd=abs(g1-g2)/765.0, bd=abs(b1-b2)/765.0;
  return (rd+gd+bd);
}

color makeRandomMainColor() {
  return color(random(colRLo,colRHi),random(colGLo,colGHi),random(colBLo,colBHi));
}


void sSetAntialias(float _x, float _y, color _c, float _p) {
  int[] aapx=new int[4];
  int[] aapy=new int[4];
  float[] aapd=new float[4];

  //antialias pixel x/y positions:
  aapx[0]=int(floor(_x)); 
  aapy[0]=int(floor(_y));
  aapx[1]=aapx[0]+1; 
  aapy[1]=aapy[0];
  aapx[2]=aapx[0]; 
  aapy[2]=aapy[0]+1;
  aapx[3]=aapx[0]+1; 
  aapy[3]=aapy[0]+1;

  float xPartLeft=1.0-(_x-aapx[0]);
  float yPartTop=1.0-(_y-aapy[0]);

  aapd[0]=xPartLeft*yPartTop;
  aapd[1]=(1.0-xPartLeft)*yPartTop;
  aapd[2]=xPartLeft*(1.0-yPartTop);
  aapd[3]=(1.0-xPartLeft)*(1.0-yPartTop);

  for (int i=0;i<4;i++) {
    int px=int(aapx[i]), py=int(aapy[i]);
    set(px%width,py%height,mixColor(_c,get(px%width,py%height),_p*aapd[i]));
  }
}

color mixColor(color c1, color c2, float p) {
  float q=1.0-p;
  return color(p*red(c1)+q*red(c2),p*green(c1)+q*green(c2),p*blue(c1)+q*blue(c2));
}
