class Particle {
  float sScale;                          // scaleFac from 0..xMax/yMax to screensize
  float x=0, y=0;                        // x/y position
  float xMax=1, yMax=1;                  // x/y maximum value (minimum is always 0)
  float xm=0, ym=0;                      // x/y movement vector
  float r=0,rd=0;                        // movement vector in radians and length
  float[] distanceToAttractor=new float[nrOfAttractors]; // distance to all attractors
  float intensity=0, targetIntensity=0;  // drawing intensity -> influences color and draw 'pressure'
  color baseCol=makeRandomMainColor();   // drawing colour
  color col=baseCol;
  float[] colDiffToAtt=new float[nrOfAttractors]; // difference between colour and attractor colour [0..1]

    // INIT PARTICLE
  Particle(int _w,int _h, float _mMin, float _mMax) {
    // find xMax and yMax; longer side is always length 1.0
    sScale=_w>_h?_w:_h;
    if (_w>_h) { 
      xMax=1.0; 
      yMax=_h/(float)_w; 
    } 
    else { 
      yMax=1.0; 
      xMax=_w/(float)_h; 
    }
    // set position and initial movement vector:
    x=random(xMax); 
    y=random(yMax);
    xm=random(_mMin,_mMax); 
    ym=random(_mMin,_mMax);
    if (random(1)<0.5) {
      xm=-xm;
    } 
    if (random(1)<0.5) {
      ym=-ym;
    }
    // set movement vector in components strength and direction:
    r=sqrt(sq(xm)+sq(ym)); 
    rd=atan2(ym,xm);
    // set array colour differences between particle and attractors
    for (int a=0;a<nrOfAttractors;a++) { 
      colDiffToAtt[a]=1.0-getColorDifference(col,attractor[a].col)/maxColorDiff; // normalized [0..1] colour difference between particle and attractor 
    }
  }

  // UPDATE POSITION, MOVEMENT OF PARTICLE
  void update() {
    // update direction and intensity if within attractor
    boolean withinOneAttractor=false;
    for (int a=0;a<nrOfAttractors;a++) {
      distanceToAttractor[a]=dist(x,y,attractor[a].x,attractor[a].y);
      // if within attractor: 
      if (distanceToAttractor[a]<attractor[a].radius) {
        // change direction according to attractor dir:
        withinOneAttractor=true; 

        float dToTurn;
        if (gTurnToColor) {
          dToTurn=getTurnDirection(rd+colDiffToAtt[a]*TWO_PI/maxColorDiff,attractor[a].dir); // change according to attractor dir and color difference
        } 
        else {
          dToTurn=getTurnDirection(rd,attractor[a].dir);
        }
        rd+=0.01*dToTurn;
        // change intensity if within attractor:
        targetIntensity+=(colDiffToAtt[a]-0.5)*0.01;
      }
    }
    if (!withinOneAttractor) {
      targetIntensity-=0.01;
    }
    targetIntensity=constrain(targetIntensity,0,1);
    intensity=(40*intensity+targetIntensity)/41.0;

    // update colour depending on intensity value
    col=mixColor(baseCol,color(255,255,255),sqrt(intensity));

    // update xm/ym-vectors if within an attractor (if change direction)
    if (withinOneAttractor) { 
      xm=r*cos(rd); 
      ym=r*sin(rd); 
    }

    // gravity only if outside 2xradius attractor and less than 20% of screen distance
    for (int a=0;a<nrOfAttractors;a++) {
      if (distanceToAttractor[a]>2*attractor[a].radius&&distanceToAttractor[a]<0.2) {
        float dx=attractor[a].x-x, dy=attractor[a].y-y;
        float s=0.0000001*attractor[a].gravityDirection/sq(distanceToAttractor[a]);
        if (gColorGravity) {
          s*=colDiffToAtt[a];
        }
        xm+=s*dx; 
        ym+=s*dy;
      }
    }
    r=sqrt(sq(xm)+sq(ym)); 
    rd=atan2(ym,xm);

    // MOVE PARTICLE TO NEW POSITION
    x+=xm; 
    y+=ym; 
    if (x<0) {
      x+=xMax;
    } 
    if (x>=xMax) {
      x-=xMax;
    }
    if (y<0) {
      y+=yMax;
    } 
    if (y>=yMax) {
      y-=yMax;
    }
  }

  // DRAW THE PARTICLE
  void drawMe(float _p) {
    sSetAntialias(x*sScale,y*sScale,col,intensity*_p);
  }
}









void initParticles(int _n,int _w, int _h,float _mMin, float _mMax) { 
  for (int i=0;i<_n;i++) { 
    particle[i]=new Particle(_w,_h,_mMin,_mMax); 
  } 
}
void updateParticlePosition(int _n) { 
  for (int i=0;i<_n;i++) { 
    particle[i].update(); 
  } 
}
void drawParticles(int _n, float _p) { 
  for (int i=0;i<_n;i++) { 
    particle[i].drawMe(_p); 
  } 
}
