class Attractor {
  int id=0;
  float sScale=400;
  float x=0,y=0;
  float xMax=0,yMax=0;
  float radius=0;
  float turn=0;
  float dir=0;
  int gravityDirection=1;
  color col=makeRandomMainColor();

  Attractor(int _id, int _w,int _h, float _radMin, float _radMax) {
    id=_id;
    sScale=_w>_h?_w:_h;
    if (_w>_h) { 
      xMax=1.0; 
      yMax=_h/(float)_w; 
    } 
    else { 
      yMax=1.0; 
      xMax=_w/(float)_h; 
    }
    radius=_radMin+sq(random(1))*(_radMax-_radMin);  // more small ones
    turn=radians(random(0,2)); 
    if (random(1)<0.5) {
      turn=-turn;
    }
    dir=random(-PI,PI);
    gravityDirection=random(1)<0.5?-1:1;

    // position circles without too much overlapping
    boolean found=false;
    while (!found) {
      x=random(xMax); 
      y=random(yMax);
      boolean overlap=false;
      for (int i=0;i<id;i++) {
        float d=dist(x,y,attractor[i].x,attractor[i].y);
        float maxR=radius>attractor[i].radius?radius:attractor[i].radius;
        if (d<maxR) {
          overlap=true;
        }
      }
      if (!overlap) {
        found=true;
      }
    }
  }

  void drawMe() {
    strokeWeight(10);
    stroke(col); 
    noFill(); 
    ellipseMode(CENTER);
    ellipse(int(x*sScale),int(y*sScale),radius*sScale,radius*sScale);
  }
}






/*
* ATTRACTOR FUNCTIONS
*/

void initAttractors(int _n,int _w, int _h, float _radMin, float _radMax) { 
  // _n:                number of Attractors
  // _w, _h:            screen width/height
  // _radMin, _radMax:  Attractor radius minimum/maximum
  for (int i=0;i<_n;i++) { 
    attractor[i]=new Attractor(i,_w,_h,_radMin,_radMax); 
  } 
}
void drawAttractors(int _n) { 
  for (int i=0;i<_n;i++) { 
    attractor[i].drawMe(); 
  } 
}
