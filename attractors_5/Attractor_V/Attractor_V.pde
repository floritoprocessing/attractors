Particle[] dot; int nr_dot;
Attractor att;
float mX,mY;

boolean clearBackground;

void setup() {
  size(500,500,P3D); background(32); stroke(255);
  nr_dot=2000; dot=new Particle[nr_dot]; for (int i=0;i<nr_dot;i++) {dot[i]=new Particle();}
  att=new Attractor(); att.setAll(0.5,0.5,1);
  clearBackground=true;
  println("move mouse");
  println("press [space] to reverse attraction");
  println("press [b] to toggle leaving traces");
}

void draw() {
  if (clearBackground) {background(32);}
  for (int i=0;i<nr_dot;i++) {
    dot[i].update();
    dot[i].toScreen();
  }
}
void mouseMoved() {
  att.setPos(mouseX/(float)width,mouseY/(float)height);
}
void keyPressed() {
  if (key==' ') {att.revDir();}
  if (key=='b'||key=='B') {clearBackground=!clearBackground;}
}

class Particle {
  float nx,ny; // new position
  float[] x,y; int lineBuf=5; // old positions
  float xm,ym; // movement vector
  
  int type;
  float[][] col={{255,224,124,32},{255,166,92,32},{255,192,22,32}};
  float[] d2Div={10000,  30000, 15000 };
  float[] minD={0.00015, 0.005, 0.01};
  float[] damp={0.96,    0.985,  0.99};
  
  Particle() {
    type=(int)random(2);
    nx=random(1); ny=random(1);
    xm=0.001*random(-1,1); ym=0.001*random(-1,1);
    x=new float[lineBuf]; y=new float[lineBuf]; for (int i=0;i<lineBuf;i++) {x[i]=nx; y[i]=ny;}
  }
  
  void update() {
    float dx=att.x-nx, dy=att.y-ny, d2=(sq(dx)+sq(dy));
    if (d2>minD[type]) { d2*=d2Div[type]; xm+=att.dir*dx/d2; ym+=att.dir*dy/d2; } else {xm/=damp[type];ym/=damp[type];}
    xm*=damp[type]; ym*=damp[type];
    nx+=xm; ny+=ym;
    for (int i=lineBuf-1;i>0;i--) {
      x[i]=x[i-1]; y[i]=y[i-1];
    } x[0]=nx; y[0]=ny;
  }
  
  void toScreen() {
    for (int i=0;i<lineBuf-1;i++) {
      stroke(col[type][0],col[type][1],col[type][2],col[type][3]);
      line(x[i]*width,y[i]*height,x[i+1]*width,y[i+1]*height);
    }
  }
}

class Attractor {
  float x,y;
  float xm,ym;
  float dir;
  Attractor() {xm=0;ym=0;}
  void setPos(float x, float y) { this.x=x; this.y=y; }
  void setDir(float dir) { this.dir=dir; }
  void revDir() { dir=-dir; }
  void setAll(float x,float y,float dir) { this.x=x; this.y=y; this.dir=dir; }
}
