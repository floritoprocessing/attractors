import processing.core.*; 
import processing.xml.*; 

import java.applet.*; 
import java.awt.*; 
import java.awt.image.*; 
import java.awt.event.*; 
import java.io.*; 
import java.net.*; 
import java.text.*; 
import java.util.*; 
import java.util.zip.*; 
import java.util.regex.*; 

public class Attractor_V_neater extends PApplet {

//import processing.opengl.*;

int PARTICLE_AMOUNT=3000;
int TRACE_LENGTH=5;

Particle[] dot;
Attractor att;
float mX,mY;

boolean clearBackground;


public void setup() {
  size(500,500,P3D); 
  background(32); 
  stroke(255);
  
  dot=new Particle[PARTICLE_AMOUNT]; 
  for (int i=0;i<PARTICLE_AMOUNT;i++) dot[i]=new Particle();
  for (int i=0;i<PARTICLE_AMOUNT;i++) dot[i].setNrOfTraceDots(TRACE_LENGTH);
  
  att=new Attractor(); 
  att.setAll(0.5f,0.5f,1);
  
  clearBackground=true;
}



public void draw() {
  if (clearBackground) background(32);
  
  for (int i=0;i<PARTICLE_AMOUNT;i++) {
    dot[i].update();
    dot[i].toScreen();
  }
}



public void mouseMoved() {
  att.setPos(mouseX/(float)width,mouseY/(float)height);
}



public void keyPressed() {
  if (key==' ') {
    att.revDir();
  }
  if (key=='b'||key=='B') {
    clearBackground=!clearBackground;
  }
}




class Attractor {
  float x,y;
  float xm,ym;
  float dir;
  Attractor() {
    xm=0;
    ym=0;
  }
  public void setPos(float x, float y) { 
    this.x=x; 
    this.y=y; 
  }
  public void setDir(float dir) { 
    this.dir=dir; 
  }
  public void revDir() { 
    dir=-dir; 
  }
  public void setAll(float x,float y,float dir) { 
    this.x=x; 
    this.y=y; 
    this.dir=dir; 
  }
}
class Particle {
  float nx,ny; // new position
  float xm,ym; // movement vector

  int NR_OF_DOTS_TO_STORE=5; // old positions
  float[] x,y; 

  int type;
  float[][] col={
    {
      255,224,124,16    }
    ,{
      255,166,92,16    }
    ,{
      255,192,22,16    }
  };
  float[] d2Div={
    10000,  30000, 3000   };
  float[] minD={
    0.00015f, 0.005f, 0.01f  };
  float[] damp={
    0.96f,    0.985f,  0.929f  };

  Particle() {
    type=(int)random(3);

    nx=random(1); 
    ny=random(1);

    xm=0.001f*random(-1,1); 
    ym=0.001f*random(-1,1);

    x=new float[NR_OF_DOTS_TO_STORE]; 
    y=new float[NR_OF_DOTS_TO_STORE]; 
    for (int i=0;i<NR_OF_DOTS_TO_STORE;i++) {
      x[i]=nx; 
      y[i]=ny;
    }
  }



  public void setNrOfTraceDots(int n) {
    NR_OF_DOTS_TO_STORE=n;
    x=new float[n];
    y=new float[n];
  }
  
  
  
  public void update() {
    float dx=att.x-nx;
    float dy=att.y-ny;
    float d2=(sq(dx)+sq(dy));

    if (d2>minD[type]){ 
      d2*=d2Div[type]; 
      xm+=att.dir*dx/d2; 
      ym+=att.dir*dy/d2; 
    } else {
      xm/=damp[type];
      ym/=damp[type];
    }
    xm*=damp[type]; 
    ym*=damp[type];
    nx+=xm; 
    ny+=ym;
    for (int i=NR_OF_DOTS_TO_STORE-1;i>0;i--) {
      x[i]=x[i-1]; 
      y[i]=y[i-1];
    } 
    x[0]=nx; 
    y[0]=ny;
  }



  public void toScreen() {
    for (int i=0;i<NR_OF_DOTS_TO_STORE-1;i++) {
      stroke(col[type][0],col[type][1],col[type][2],col[type][3]);
      line(x[i]*width,y[i]*height,x[i+1]*width,y[i+1]*height);
    }
  }
}

  static public void main(String args[]) {
    PApplet.main(new String[] { "--bgcolor=#ece9d8", "Attractor_V_neater" });
  }
}
