<div id="Attractor_V_neater_container">
			
			<!--[if !IE]> -->
				<object classid="java:Attractor_V_neater.class" 
            			type="application/x-java-applet"
            			archive="Attractor_V_neater.jar"
            			width="500" height="500"
            			standby="Loading Processing software..." >
            			
					<param name="archive" value="Attractor_V_neater.jar" />
				
					<param name="mayscript" value="true" />
					<param name="scriptable" value="true" />
				
					<param name="image" value="loading.gif" />
					<param name="boxmessage" value="Loading Processing software..." />
					<param name="boxbgcolor" value="#FFFFFF" />
				
					<param name="test_string" value="outer" />
			<!--<![endif]-->
				
				<object classid="clsid:8AD9C840-044E-11D1-B3E9-00805F499D93" 
						codebase="http://java.sun.com/update/1.5.0/jinstall-1_5_0_15-windows-i586.cab"
						width="500" height="500"
						standby="Loading Processing software..."  >
						
					<param name="code" value="Attractor_V_neater" />
					<param name="archive" value="Attractor_V_neater.jar" />
					
					<param name="mayscript" value="true" />
					<param name="scriptable" value="true" />
					
					<param name="image" value="loading.gif" />
					<param name="boxmessage" value="Loading Processing software..." />
					<param name="boxbgcolor" value="#FFFFFF" />
					
					<param name="test_string" value="inner" />
					
					<p>
						<strong>
							This browser does not have a Java Plug-in.
							<br />
							<a href="http://java.sun.com/products/plugin/downloads/index.html" title="Download Java Plug-in">
								Get the latest Java Plug-in here.
							</a>
						</strong>
					</p>
				
				</object>
				
			<!--[if !IE]> -->
				</object>
			<!--<![endif]-->
			
			</div>

<p></p>
<p>Press [SPACE] to invert attraction.<br />
  Press [B] to set tracing on/off.
 </p>
<p>
Source code: <a href="Attractor_V_neater.pde">Attractor_V_neater</a> <a href="Attractor.pde">Attractor</a> <a href="Particle.pde">Particle</a> 
</p>

<p>
Built with <a href="http://processing.org" title="Processing.org">Processing</a>
</p>