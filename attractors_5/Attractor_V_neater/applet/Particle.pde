class Particle {
  float nx,ny; // new position
  float xm,ym; // movement vector

  int NR_OF_DOTS_TO_STORE=5; // old positions
  float[] x,y; 

  int type;
  float[][] col={
    {
      255,224,124,16    }
    ,{
      255,166,92,16    }
    ,{
      255,192,22,16    }
  };
  float[] d2Div={
    10000,  30000, 3000   };
  float[] minD={
    0.00015, 0.005, 0.01  };
  float[] damp={
    0.96,    0.985,  0.929  };

  Particle() {
    type=(int)random(3);

    nx=random(1); 
    ny=random(1);

    xm=0.001*random(-1,1); 
    ym=0.001*random(-1,1);

    x=new float[NR_OF_DOTS_TO_STORE]; 
    y=new float[NR_OF_DOTS_TO_STORE]; 
    for (int i=0;i<NR_OF_DOTS_TO_STORE;i++) {
      x[i]=nx; 
      y[i]=ny;
    }
  }



  void setNrOfTraceDots(int n) {
    NR_OF_DOTS_TO_STORE=n;
    x=new float[n];
    y=new float[n];
  }
  
  
  
  void update() {
    float dx=att.x-nx;
    float dy=att.y-ny;
    float d2=(sq(dx)+sq(dy));

    if (d2>minD[type]){ 
      d2*=d2Div[type]; 
      xm+=att.dir*dx/d2; 
      ym+=att.dir*dy/d2; 
    } else {
      xm/=damp[type];
      ym/=damp[type];
    }
    xm*=damp[type]; 
    ym*=damp[type];
    nx+=xm; 
    ny+=ym;
    for (int i=NR_OF_DOTS_TO_STORE-1;i>0;i--) {
      x[i]=x[i-1]; 
      y[i]=y[i-1];
    } 
    x[0]=nx; 
    y[0]=ny;
  }



  void toScreen() {
    for (int i=0;i<NR_OF_DOTS_TO_STORE-1;i++) {
      stroke(col[type][0],col[type][1],col[type][2],col[type][3]);
      line(x[i]*width,y[i]*height,x[i+1]*width,y[i+1]*height);
    }
  }
}
