class Attractor {
  float x,y;
  float xm,ym;
  float dir;
  Attractor() {
    xm=0;
    ym=0;
  }
  void setPos(float x, float y) { 
    this.x=x; 
    this.y=y; 
  }
  void setDir(float dir) { 
    this.dir=dir; 
  }
  void revDir() { 
    dir=-dir; 
  }
  void setAll(float x,float y,float dir) { 
    this.x=x; 
    this.y=y; 
    this.dir=dir; 
  }
}
