//import processing.opengl.*;

int PARTICLE_AMOUNT=3000;
int TRACE_LENGTH=5;

Particle[] dot;
Attractor att;
float mX, mY;

boolean clearBackground;
boolean SAVE_FRAMES = false;


void setup() {
  size(500, 500, P3D); 
  background(32); 
  stroke(255);

  dot=new Particle[PARTICLE_AMOUNT]; 
  for (int i=0; i<PARTICLE_AMOUNT; i++) dot[i]=new Particle();
  for (int i=0; i<PARTICLE_AMOUNT; i++) dot[i].setNrOfTraceDots(TRACE_LENGTH);

  att=new Attractor(); 
  att.setAll(0.5, 0.5, 1);

  clearBackground=true;
}



void draw() {
  if (clearBackground) background(32);

  for (int i=0; i<PARTICLE_AMOUNT; i++) {
    dot[i].update();
    dot[i].toScreen();
  }

  if (SAVE_FRAMES)
    if ((frameCount-1)%2==0) saveFrame("D:\\Processing visual index\\"+getClass().getName()+"\\"+getClass().getName()+"_#####.jpg");
}



void mouseMoved() {
  att.setPos(mouseX/(float)width, mouseY/(float)height);
}



void keyPressed() {
  if (key==' ') {
    att.revDir();
  }
  if (key=='b'||key=='B') {
    clearBackground=!clearBackground;
  }
}
