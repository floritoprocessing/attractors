class Planet {
  
  private float MASS = 1.0;
  private Vec pos;
  
  Planet(float _x, float _y, float _m) {
    pos = new Vec(_x,_y);
    MASS = _m;
  }
  
  Vec getGravityVecAt(Vec partPos) {
    Vec partToMe = new Vec(vecSub(pos,partPos));
    float distance = partToMe.getLength();
    float F = MASS/distance;
    partToMe.normalizeTo(F);
    return partToMe;
  }
  
  void draw() {
    ellipseMode(CENTER);
    noFill();
    stroke(255,0,0);
    ellipse(pos.getX(),pos.getY(),2*MASS,2*MASS);
  }
  
}
