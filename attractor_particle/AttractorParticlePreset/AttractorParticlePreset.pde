import java.util.Vector;

int PLANETS_AMOUNT = 10;
int PARTICLE_AMOUNT = 100;

Vector planets, particles;

boolean drawPlanets = false;

void setup() {
  size(640,480,P3D);
  planets = new Vector();
  particles = new Vector();
  for (int i=0;i<PLANETS_AMOUNT;i++) planets.add(new Planet(random(width),random(height),random(1.0,10.0)));
  for (int i=0;i<PARTICLE_AMOUNT;i++) particles.add(new Particle(random(width),random(height)));
  background(0,0,0);
  println("[p] toggle drawing planets");
  println("press mouse to reset");
}

void keyPressed() {
  if (key=='p') drawPlanets=!drawPlanets;
}

void mousePressed() {
  setup();
}

void draw() {
  //background(0,0,0);
  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).update(planets);
  for (int i=0;i<particles.size();i++) ((Particle)particles.elementAt(i)).draw();
  if (drawPlanets) for (int i=0;i<planets.size();i++) ((Planet)planets.elementAt(i)).draw();
}
