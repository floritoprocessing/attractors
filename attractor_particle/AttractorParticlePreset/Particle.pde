class Particle {
  
  Vec pos;
  Vec mov;
  
  Particle(float _x, float _y) {
    pos = new Vec(_x,_y);
    mov = new Vec();
  }
  
  void update(Vector p) {
    Vec acc = new Vec();
    
    for (int i=0;i<p.size();i++) {
      acc.add(((Planet)p.elementAt(i)).getGravityVecAt(pos));
    }
    
    mov.add(acc);
    pos.add(mov);
    
  }
  
  void draw() {
    stroke(128,128,255,128);
    point(pos.getX(),pos.getY());
  }
  
}
