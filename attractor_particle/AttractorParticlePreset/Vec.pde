class Vec {

  private float x=0, y=0, z=0;

  Vec() {
  }

  Vec(float _x, float _y) {
    x = _x;
    y = _y;
  }

  Vec(float _x, float _y, float _z) {
    x = _x;
    y = _y;
    z = _z;
  }
  
  Vec(Vec v) {
    x = v.getX();
    y = v.getY();
    z = v.getZ();
  }

  public float getX() { 
    return x; 
  }
  public float getY() { 
    return y; 
  }
  public float getZ() { 
    return z; 
  }
  
  public float getLength() {
    return sqrt(x*x+y*y+z*z);
  }




  public void setX(float _x) { 
    x = _x; 
  }
  public void setY(float _y) { 
    y = _y; 
  }
  public void setZ(float _z) { 
    z = _z; 
  }
  
  public void add(Vec b) {
    x += b.getX();
    y += b.getY();
    z += b.getZ();
  }
  
  public void mul(float m) {
    x *= m;
    y *= m;
    z *= m;
  }
  
  public void normalizeTo(float f) {
    float d = this.getLength();
    if (d!=0) {
      float scaleFac = f/d;
      this.mul(scaleFac);
    }
  }
  

}

Vec vecSub(Vec a, Vec b) {
  return new Vec(a.getX()-b.getX(),a.getY()-b.getY(),a.getZ()-b.getZ());
}
